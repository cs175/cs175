\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper, total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}

%\linespread{2}
\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
%\titleformat*{\subsection}
\setlength\parindent{0pt}

\title{Do Lyrics Classify Music Genre?}
\author{Brad Rafferty (7191822)\\%, braffert@uci.edu\\
Darpan Patel (35198867)\\%, darpanp@uci.edu\\
Heliodoro Tejeda (000823989)}%, htejeda@uci.edu}
\date{}

\begin{document}

\maketitle
\tableofcontents

\clearpage

\section{Project Summary}

The problem we will address in this project is genre classification based on
song lyrics. We plan on using datasets from the Million Song Dataset and
achieving this goal through the use of different classifiers. The main goal is
to determine what words (if any) pertain to what genre.

\section{Background / Description}

Music can be broken down into two main components, instrumentals and lyrics.
Instrumentals alone can stand as music, however lyrics alone are classified as
poetry. Thus not all music has lyrics, but all lyrics that are accompanied by
instrumentals are music. Now, the question remains, is genre classified by
instrumentals or by lyrics?\\
\\
This problem has been previously attempted with mixed
results which we hope to improve upon
(http://cs229.stanford.edu/proj2012/Diekroeger-CanSongLyricsPredictGenre.pdf).
\\
\\
We believe we can increase the accuracy with a Multinomial Naive Bayes
approach. While adding uninformative features will actually decrease accuracy in
some cases, we believe that including the frequency of word appearance, like in
Multinomial Naive Bayes, is of key importance in song lyrics. We also believe that using
Logistic Regression will improve upon the accuracy because song lyrics are not
the type of data that can be easily linearly separable.\\
\\
Overall we feel there is a large amount of room for improvement since the
previously attempted experiment had such disappointing results.



\section{Data Sets}

For this project we plan to use two datasets which come from the Million Song
Dataset:
\begin{itemize}
  \item musiXmatch, (http://labrosa.ee.columbia.edu/millionsong/musixmatch)
  \item Last.fm, (http://labrosa.ee.columbia.edu/millionsong/lastfm)
\end{itemize}

MusiXmatch has 237,662 different music tracks, with a total of 55,163,335
occurrences and 498,134 unique words. The songs are
provided in a bag-of-words format and are already divided into training and test
sets as merely a suggestion. We will concatenate the two files and choose our
own train and test data.\\
\\
The Last.fm dataset (provided in a JSON file) contains 505,216 songs with the
necessary tags for us to correctly identify the song's genre.
Since the Last.fm dataset is given by user tags, we will have to limit the
number of song genres to the most popular and relevant categories. This is to
avoid obscure genres that might only be tagged for a
handful of songs. The log graph in Fig. 1 shows how only a handful of genre tags
will actually be relevant.\\
\\
%\begin{figure}[ht]
%  \centering
%  \begin{subfigure}{0.38\textwidth}
%    \centering
%    \includegraphics[width=\textwidth, height=0.1\textheight]{pics/genres.png}
%  \end{subfigure}
%  \hfill
%  \begin{subfigure}{0.6\textwidth}
%    \centering
%    \includegraphics[width=\textwidth, height=0.1\textheight]{pics/freq.png}
%  \end{subfigure}
%  \caption{Last.fm Genre and Tag Frequencies}
%\end{figure}
Ultimately we will have to cross reference the lyrics given in the MusiXmatch
with those in the Last.fm dataset. Thus, the data set we will be working with
is the intersection of the two sets (illustrated below).

\begin{figure}[ht]
  \centering
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth, height=0.1\textheight]{pics/freq.png}
    \caption{Last.fm Genre Tag Freq}
  \end{minipage}
  \hfill
  \begin{minipage}[b]{0.4\textwidth}
    \includegraphics[width=\textwidth, height=0.1\textheight]{pics/data.png}
    \caption{Final Data Set for Project}
  \end{minipage}
\end{figure}


\section{Technical Approach}

We will be using Python as our primary language. We will use the SQLite library
to access the database information we plan to use, the NLTK library for any
additional filtering of that information, and finally the SciKit library for
quick access to the classifying algorithms. Our aim is to create a pipeline that
can be fed the data and classifier, then interpret our results. A figure of this
pipeline is illustrated bellow.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth, height=0.1\textheight]{pics/pipeline.png}
  \caption{Pipeline Illustration of the Code we will write}
\end{figure}

\subsection{Classifying Algorithms}

The three classifying algorithms we will use are \textit{Bernoulli Naive Bayes},
\textit{Multinomial Naive Bayes}, and \textit{Logistic Regression}. We expect the
\textit{Bernoulli Naive Bayes} classifier to do better when we are only classifying
two genres at a time, as opposed to an entire array of genres. However, we expect
to see the \textit{Multinomial Naive Bayes}
and \textit{Logistic Regression} classifiers to perform better when there are multiple
genres.

\section{Experiments and Evaluation}

We plan on conducting two different experiments using three different
classifiers. These classifiers include: \textit{Bernoulli Naive Bayes},
\textit{Multinomial Naive Bayes}, and \textit{Logistic Regression}. Our first
experiment will use the word
counts, while our second will eliminate word counts and only use binary values.
We are hoping that there will be a clear distinction between both approaches.\\
\\
Although our data comes in already established train and test sets, we plan on
combining these two sets and creating our own train and test sets. With this we
will use cross-validation to estimate our errors.\\
\\
Once we have arrive at our results, we plan on diving into the classifiers to
see what caused particular choices. In essence, we would like to see which words
are more common in what genre (i.e. Country vs Rap).

\section{Software}

The primary software we will use is the Anaconda implementation of Python. This
comes with the publicly available libraries we will be using: $NLTK$, $SciKit$,
and $SQLite$.\\
\\
In addition to these libraries, we will also be using the musixmatch API as well
as the Last.fm API.\\
\\
All other software will be code that we write including (but not
limited to) our classifier pipeline as well as a scoring algorithm to measure
accuracies.

\section{Milestones}

\begin{figure}[ht]
  \centering
  \begin{tabular}{|c|p{13cm}|}
    \hline

    Week 5 \& 6 & During these weeks we hope to download the data sets described
    in Sec. 3 and learn how to read and manipulate them. Since last.fm is
    provided as a SQLite database it might take some time to learn how to
    extract the correct information in Python and be able to cross reference
    them appropriately. \\ \hline

    Week 7 \& 8 & After obtaining the relevant information to work with, we will
    start the coding for our project and build an appropriate pipeline to
    process the information. Hopefully by this point, we will be able to see
    if our original assumptions that the Multinomial and Logistic
    Regression approaches were the better options. Also during
    this time, we will be working on our progress report because it is due
    Monday of week 8. \\ \hline

    Week 9 \& 10 & At this stage we will be able to start applying different
    techniques and learning what works and what does not. We might try
    tweaking what genres we are working with. For instance, rock might be too
    open ended to work in, since it’s such a broad genre. A genre like country
    however should be more closed and contain a more confined language style. We
    will also be working on both our presentation and our final progress report.  \\
    \hline

  \end{tabular}
\end{figure}

\section{Individual Student Responsibilities}
\begin{figure}[ht]
  \centering
  \begin{tabular}{|c|p{13cm}|}
    \hline
    % Brad
    Brad & Write the pipelines which will allow the classifiers to work properly
    on our datasets. Will assist in doing experiments and interpreting
    results. Will assist in writing project reports.\\ \hline

    % Darpan
    Darpan & Setup and manage the initial database files. Will assist in doing
    experiments and interpreting results. Will also assist in writing project
    reports. \\ \hline

    % Helio
    Helio & Setup and manage the Bitbucket account hosting the project
    repository. Will
    assist in doing experiments and interpreting results. Will format project
    reports. \\ \hline

  \end{tabular}
\end{figure}



\end{document}
