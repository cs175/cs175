\contentsline {section}{\numberline {1}Background / Description}{2}{section.1}
\contentsline {section}{\numberline {2}Description of Technical Approach}{2}{section.2}
\contentsline {subsection}{\numberline {\textbf {2.a}}Classifying Algorithms}{3}{subsection.2.1}
\contentsline {section}{\numberline {3}Data Sets}{3}{section.3}
\contentsline {section}{\numberline {4}Software}{5}{section.4}
\contentsline {subsection}{\numberline {\textbf {4.a}}Code Written}{5}{subsection.4.1}
\contentsline {subsection}{\numberline {\textbf {4.b}}Code/Scripts Successfully Used on Data}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {\textbf {4.c}}Software we Plan to Write/Use}{5}{subsection.4.3}
\contentsline {section}{\numberline {5}Experiments and Evaluation}{5}{section.5}
\contentsline {subsection}{\numberline {\textbf {5.a}}Completed Experiments}{5}{subsection.5.1}
\contentsline {subsection}{\numberline {\textbf {5.b}}Planned Experiments}{5}{subsection.5.2}
\contentsline {section}{\numberline {6}Challenges Identified}{6}{section.6}
\contentsline {subsection}{\numberline {\textbf {6.a}}Data Challenges}{6}{subsection.6.1}
\contentsline {subsection}{\numberline {\textbf {6.b}}Classifier Challenges}{6}{subsection.6.2}
\contentsline {section}{\numberline {7}Updated Milestones}{7}{section.7}
\contentsline {section}{\numberline {8}Individual Student Accomplishments}{7}{section.8}
