\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper, total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}
\usepackage[colorlinks]{hyperref}
\hypersetup{linkcolor=blue}
\hypersetup{urlcolor=blue}

\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
\newcommand{\tabitem}{$\bullet$  }
\setlength\parindent{0pt}

\title{Do Lyrics Classify Music Genre?}
\author{Brad Rafferty, (7191822), braffert@uci.edu\\
Darpan Patel, (35198867), darpanp@uci.edu\\
Heliodoro Tejeda, (000823989), htejeda@uci.edu}
\date{}

\begin{document}

\maketitle
\tableofcontents

\clearpage

%\section{Project Summary}

%The problem we will address in this project is genre classification based on
%song lyrics. We plan on using datasets from the Million Song Dataset and
%achieving this goal through the use of different classifiers. The main goal is
%to determine what words (if any) pertain to what genre.

\section{Background / Description}

Music can be broken down into two main components, instrumentals and lyrics.
Instrumentals alone can stand as music, however lyrics alone are classified as
poetry. Thus not all music has lyrics, but all lyrics that are accompanied by
instrumentals are music. Now, the question remains, is genre classified by
instrumentals or by lyrics?\\
\\
The problem we are addressing in this project is genre classification based on
song lyrics. This problem has been
\href{http://cs229.stanford.edu/proj2012/Diekroeger-CanSongLyricsPredictGenre.pdf}{previously
attempted} with mixed results which we hope to improve upon. This project used
the basic data provided by MusiXmatch and classified with Naive Bayes and
Support Vector Machines. We plan on improving this by using Multinomial Bayes
Classification with better data, as well as trying new approaches such as
K-Nearest Neighbors and Decision Trees. \\
\\
We are only using the dataset provided by
\href{http://labrosa.ee.columbia.edu/millionsong/musixmatch}{MusiXmatch}, for
the track id's. From here, we have began to construct a more fulfilled dataset
through API calls to MusiXmatch and web scraping to gather complete lyrics. The
data collection process is described in more detail in Section 3. As of now,
3,471 of the songs we have scraped do not have a genre associated to them. Using
the songs that do (19,480), we are hoping to come up with reasonable estimates
for the unlabeled set.\\
\\
Overall we feel there is room for improvement since the previously attempted
experiment had such disappointing results.



\section{Description of Technical Approach}

The original dataset we began working with provided a default bag-of-words for
237,662 different music tracks. We decided against using this bag-of-words since
it broke words down into their most basic form (e.g. frustrate or frustrating
became frustrat). The default data also removed all punctuation including
apostrophes. We believe that these removals took away from contextual
information since each word in a song is picked in a meaningful manner (e.g.
hanging might be more likely to appear in a pop song, while hangin' might be
more likely to appear in a rap song). After improving the data, we plan to apply
the updated bag-of-words to three classifiers described below.\\
\\
We are still using Python as our primary language. We will use API's to collect
our data (described in Section 3), the NLTK library for any text filtering and
the creation of our bag-of-words, Scikit-Learn for our classification
algorithms, and any further open-source work that we find to be useful.  Our aim
is to create a pipeline that can be fed the data and classifier, then interpret
our results. A figure of this pipeline is illustrated bellow.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth, height=0.1\textheight]{pics/pipeline.png}
  \caption{Pipeline Illustration of the Code we will write}
\end{figure}

\subsection{Classifying Algorithms}

Multinomial Bayes is our safety net since we have already worked with the
classifier before. Bayes algorithms consider each value independent from every
other value. Each data point, or song word, will contribute independently to the
probability of the song genre. Given the words of a song, a weight is given to
each individual word and a resulting value is calculated to predict the genre.\\
\\
K-Nearest Neighbors is a way of predicting a classification based off
of similar groupings. Songs would be plotted to different points based on their
genre score. To classify a new song, you would then compute which genre has the
K-nearest neighbors to our new song. Thus new songs are based on where they
appear on the plot and their surrounding neighbors.\\
\\
Decision Trees would try to sort things by whether or not certain words could
break things into different categories. For instance there are 1000 documents
with 90\% of those documents belonging to one category and 10\% belonging to another.
A maximum gain would be if a word always appeared in the 10\% set, but never in
the other. That could perfectly split the original group into 100/100 ``word"
appears, and 0/900 ``word never appears".


\section{Data Sets}

We originally began this project working with the MusiXmatch dataset provided by
\href{http://labrosa.ee.columbia.edu/millionsong/musixmatch}{Columbia
University's Million Song Dataset}. This dataset provided us with a total of
237,662 music tracks. However, we found that the dataset was lacking in information
and missing a lot of data. For a better description of the problems
faced with our original dataset, see Section 6.a Data Challenges.\\
\\
To resolve our dataset problem, we engineered a new strategy. After some
investigation, we found that the music tracks provided by the Million Song
Dataset had track-id's that could be used with the MusiXmatch API. Through the
use of a Python API Wrapper, the API provided us with a lot of metadata about
different tracks. The image below shows the data we have collected for each
track in our dataset (lyrics are omitted due to copyright).

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.5\textwidth]{pics/song_data.png}
    \caption{Sample of Song Data Collected}
\end{figure}

Unfortunately, there were a few problems that we
encountered while using the API. Below are the problems and their solutions.

\begin{enumerate}

    \item \textbf{Problem: }The Python API Wrapper was implemented with Python
    2.7, which caused errors when run with Python 3.5.\\

    \textbf{Solution: }We re-wrote the Wrapper for Python 3.5. While
    re-writing it, we improved some of the functions and customized some aspects
    for our purposes.

    \item \textbf{Problem: }The bag-of-words that was provided by the API
    were preprocessed. This meant that words were cut down to their stems and
    all contractions were disassembled. In music, we believe that it is not just
    the words that are used, but how they are used that helps determine
    genre.\\

    \textbf{Solution: }We decided rather than using the bag-of-words
    provided, we would create a web scraper that would grab the lyrics from the
    url provided by the API. We know that the words found by this URL are the
    unprocessed words that were found in the bag-of-words. This gave us the
    ability to process the lyrics as we wished and create our own bag-of-words.

    \item \textbf{Problem: }We created a script which would iterate through all
    of the 237,662 tracks provided, make an API call for each track, determine
    whether the track was in English, and if so gather the information we wanted
    (this includes the web scraping). It then stored the data into appropriately
    labeled files (described later). Unfortunately, after leaving the program
    running overnight (since the process was extremely time consuming) we
    discovered that the API keys we created had a limited number of API
    calls for free users. It had cut off at only 5,000 songs, far from our track
    id data of 237,662 tracks.\\

    \textbf{Solution: }Ultimately, we created various accounts giving us
    different API keys to use. We then updated our program to swap API keys once
    one had expired. Unfortunately, the process is still very time consuming. As
    of now, we have only been able to iterate over 55,110 tracks.

    \item \textbf{Problem: }Once running the program more and more, we
    discovered that there are problems with some of the id's provided. This
    includes tracks that were not in English, faulty track id's, tracks that did
    not have lyrics (instrumental songs), tracks that had no genre, and tracks
    that had multiple genres. Finally, there were some time-out issues (while
    accessing the API or retrieving lyrics) that would occur from time to
    time.\\

    \textbf{Solution: }To solve all of these problems, we filled our program
    with try's and except's to catch these errors and created several different
    files to organize the data. For the tracks that were not in English, we
    would write their track id to $no\_en.txt$. For non-functioning id's, we
    write their track id to $error.txt$. For instrumental tracks, we write their
    track id to $no\_lyrics.txt$. For tracks that did not have a genre, but had
    the rest of the information we wanted, we would write the information to
    $find\_genre.json$ (this is the data we will test on). For tracks with
    multiple genres and all other information we seek, we write the information
    to $narrow\_genre.json$. We will also use these tracks during our experiments
    (process is described in Section 5 Experiments and Evaluation). Finally, for
    time-outs, we write the track id to $redo.txt$. We plan on running the
    program again using these redo id's to collect more data.

\end{enumerate}

Although we are still collecting our data, we do have statistics for the 55,110
tracks that we have iterated over. We plan on using the no genre data as our
testing data and our main data as our training data.\\
\\
\begin{tabular}{|c|c|c|c|c|c|c|}
    \hline
    Non-English & Errors & Instrumental & Redo &
    No Genre & Multiple Genres & Main Data
    \\ \hline
    6,797 & 5,975 & 312 & 10,547 & 3,471 & 8,497 & 19,480
    \\ \hline
\end{tabular}


\section{Software}

\subsection{Code Written}

We wrote a script that uses the original MusiXmatch dataset. It uses the
MusiXmatch API to gather metadata and scrapes the MusiXmatch website for lyrics.
This is done by using the track ID information provided in the original
dataset. This code then sorts the data into either a $json$ file that has genre
information, a $json$ file that has no genre information, a $json$ file that has
multiple genres, and a few other files that are described in Section 3.

\subsection{Code/Scripts Successfully Used on Data}

We wrote a script that takes the complete lyrics found in our $json$ files for
each song and converts them into a bag-of-words. We did this by using the NLTK
library provided by the Anaconda version of Python and using the pre-built
functions to tokenize the lyrics, remove the stopwords, and get a bag of words.

\subsection{Software we Plan to Write/Use}

We plan on writing the necessary scripts to construct our pipeline. These
scripts will include the classification algorithms: Multinomial Bayes, K-Nearest
Neighbors, and Decision Trees. Additionally based off of the best classifier
we will program a way to give tentative genre labels for all the song lyrics on
MusiXmatch which do not have them.


\section{Experiments and Evaluation}


\subsection{Completed Experiments}

Since the original dataset we were using did not pan out, we were forced to
build a web scraper and compile the data into the format we wanted ourselves.
This put us behind schedule and we have not completed any experiments. However,
now we do have the exact data we want to work with, so conducting the experiment
should be relatively easy now.

\subsection{Planned Experiments}

We plan on using three different classifiers in our experiment: Multinomial
Bayes, k-Nearest Neighbors, and Decision Trees. We will test their accuracy by
dividing the songs we have with genres into training (15\%) and test data
(85\%). Then using the Scikit library which provides ways of building and
analyzing all three of these models (MultinomialNB(), KNeighborsClassifier(),
DecisionTreeClassifier()), we will build the models and get a percent accuracy.
If the accuracy is too low still, we will try to change the number of
classifying labels we are using and drop genres from the dataset that might be
too specific. Ultimately, we will take these models and apply them to the data
that are still missing genres. We will then check a number of the songs by hand
to see how well the classifiers are doing.\\
\\
Once we have arrived at our results, we plan on diving into the classifiers to
see what caused particular choices. In essence, we would like to see which words
are more common in what genre (i.e. Country vs Rap).


\section{Challenges Identified}

\subsection{Data Challenges}

Originally, we found
\href{http://labrosa.ee.columbia.edu/millionsong/musixmatch}{Columbia
University's Million Song Dataset} which gave us two $txt$ files (one training
and one test) of songs already in a bag-of-words format. Combining both files,
this gave us a grand total of 237,662 songs for our data. Although genre was not
included in the files, the site stated that we could use their
\href{http://labrosa.ee.columbia.edu/millionsong/lastfm}{Last.fm Million Song
Dataset} to cross-reference the genre of each song. Unfortunately, there were
several different problems we identified with the files and the
cross-referencing.

\begin{enumerate}

    \item Not all of the songs provided were in English. Some of the other
    languages included: Spanish, French, German, Portuguese, Dutch.

    \item The site states that when considering all songs, there are 498,134
    unique words. However, all of the bag-of-words for the songs provided only
    account for the top 5,000 words used, a lot of which were non-English words.
    Thus, there were songs that only had 3 words in their bag-of-words. This is
    because the rest of the words in that song were not found in the top 5,000
    words used. Thus, we had a huge absence of data.

    \item We found that cross referencing was impossible. The Last.fm
    dataset only provided a list of genre tags and how many times they
    appeared, with no indication of what song they came from. We originally
    attempted to solve this by joining the Last.fm research group, but this
    proved too difficult.

\end{enumerate}

Once we identified these problems, we determined another manner in which to
collect our data. This process is described in Section 3, Data Sets.

\subsection{Classifier Challenges}

It was pointed out that the classifiers we were originally going to use
(Bernoulli Naive Bayes, Multinomial Naive Bayes, and Logistic Regression) were
too simplistic (an exact copy of Homework 2). In addition, the manner in which
we were going to use Bernoulli Naive Bayes was incorrect. Thus, we needed to
identify new classifiers to use and ensure that we would be using them
correctly. This correction is explained in more detail in Section 2 Description
of Technical Approach.

\clearpage


\section{Updated Milestones}

\begin{figure}[ht]
  \centering
  \begin{tabular}{|c|p{13cm}|}
    \hline

    Week 8 &

    We will start the coding for our pipeline to process and interpret our data.
    This pipeline will take a track as input and return the results for all
    classifiers we are working with. We will also begin to work on and finish a
    powerpoint for the class presentation.

    \\ \hline

    Week 9 &

    At this point we will have our pipeline implemented and will begin to debug
    any problems that may arise. We will also begin to interpret our results for
    the songs we classified. This will randomly selecting 10\% of the test songs
    (which come from our unlabeled genre data) to determine the accuracy of our
    classifiers.

    \\ \hline

    Week 10 &

    We will run through some final experiments and interpret the results. These
    last runs will be special case runs, mostly for fun (this means we
    will pick poems, maybe some of Shakespeare's work or Edgar Allan Poe's, and see
    what music genres they would be classified under). In addition we will work
    on and finish the final report.

    \\ \hline

  \end{tabular}
\end{figure}

\section{Individual Student Accomplishments}
\begin{figure}[ht]
  \centering
  \begin{tabular}{|c|p{13cm}|}
    \hline
    % Brad
    Brad &

    \begin{minipage}[t]{13cm}
        \begin{itemize}
            \setlength{\itemsep}{0pt}
            \setlength{\parskip}{0pt}
            \setlength{\parsep}{0pt}
            \item Tried to work with the original databases listed in the
    project proposal, only to find out there was no way to correlate the last.fm
    dataset (which had the genres) with the MusicXmatch dataset (which had the
    lyrics). Struggled to join the last.fm research group; decided against using
    the original MusicXmatch bag of words.
            \item Generated API keys for MusicXmatch by making a series of dummy
    accounts.
            \item Used the NLTK library to process the lyrics obtained from web
    scraping.
            \item Wrote Project Proposal/Progress Report (60\%).
        \end{itemize}
    \end{minipage}

    \\ \hline

    % Darpan
    Darpan &

    \begin{minipage}[t]{13cm}
        \begin{itemize}
            \setlength{\itemsep}{0pt}
            \setlength{\parskip}{0pt}
            \setlength{\parsep}{0pt}
            \item Attempted to write a web scraper. (15\%)
            \item Wrote Project Proposal/Progress Report. (15\%)
        \end{itemize}
    \end{minipage}

    \\ \hline

    % Helio
    Helio &

    \begin{minipage}[t]{13cm}
        \begin{itemize}
            \setlength{\itemsep}{0pt}
            \setlength{\parskip}{0pt}
            \setlength{\parsep}{0pt}
            \item Setup the git repository for the project group, and taught Brad
    and Darpan how to use it.
            \item Primary writer of the web scraper for MusicXmatch, which ended
    up giving us the necessary data to work with for our project. (85\%)
            \item Wrote Project Proposal/Progress Report (25\%).
            \item Formatted Project Proposal/Progress Report.
        \end{itemize}
    \end{minipage}

    \\ \hline

  \end{tabular}
\end{figure}



\end{document}
