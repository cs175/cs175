\normalfont\documentclass[a4paper, 12pt]{article}
\usepackage[a4paper, total={6.5in, 10in}]{geometry}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{comment}
\usepackage[colorlinks]{hyperref}
\usepackage{wrapfig}
\hypersetup{linkcolor=blue}
\hypersetup{urlcolor=blue}

\renewcommand{\thesubsection}{\textbf{\thesection.\alph{subsection}}}
\newcommand{\tabitem}{$\bullet$  }
\setlength\parindent{0pt}

\title{Do Lyrics Classify Music Genre?}
\author{Brad Rafferty, (7191822), braffert@uci.edu\\
Heliodoro Tejeda, (000823989), htejeda@uci.edu}
\date{}

\begin{document}

\maketitle
\tableofcontents

\clearpage

%\section{Project Summary}

%The problem we will address in this project is genre classification based on
%song lyrics. We plan on using datasets from the Million Song Dataset and
%achieving this goal through the use of different classifiers. The main goal is
%to determine what words (if any) pertain to what genre.

%
%   Background and Description
%
\section{Background / Description}

Music can be broken down into two main components, instrumentals and lyrics.
Instrumentals alone can stand as music, however lyrics alone are classified as
poetry. Thus not all music has lyrics, but all lyrics that are accompanied by
instrumentals are music. Now, the question remains, is genre classified by
instrumentals or by lyrics?\\
\\
When listening to the radio, it is easy to identify a song's genre based on
what is heard. However, we were interested in knowing if that same
classification would be just as easy (or even possible) after removing the
sound. The goal of this project was to strip away instrumentals from music and
leave only the words. Thus, we attempted to classify music genres based
solely on lyrics.


%
%   Related Work
%
\section{Related Work}

This problem has been previously attempted various times with mixed results.
In particular (while conducting our research) we stumbled upon two sources
that had distinctly different results. The
{\href{http://cs229.stanford.edu/proj2012/Diekroeger-CanSongLyricsPredictGenre.pdf}
    {first}
}
project used the basic data provided by the \textit{Million Song Dataset} and
classified with Naive Bayes and Support Vector Machines. Ultimately, the
project only did marginally better than a random classifier (classified
correctly between 20-25\%).\\
\\
The
{\href{http://cs229.stanford.edu/proj2012/BourabeeGoMohan-ClassifyingTheSubjectiveDeterminingGenreOfMusicFromLyrics.pdf}
    {second}
}
project had a huge improvement over the first. Their data consisted of two
different sets; one coming from the \textit{Million Song Dataset} (similar
results to the first project) and the other from a publicly available dataset
provided by UPenn (better results). To produce these better results, the
project incorporated different classification techniques such as: Naive Bayes,
Multi-class SVM, Softmax Regression, K-Nearest Neighbors, and  k-Means. The
project had a peak accuracy classification at 48\%.\\
\\
We hoped to continue this trend of improvement through the adaptation of new
classification methods and a new dataset (described in Section 3). These
classification methods include: Multinomial Naive Bayes, K-Nearest Neighbors,
Decision Trees, and One vs. Rest using Linear Support Vector Classification
(Linear SVC).


%
%   Data Sets
%
\section{Data Sets}
We originally began this project working with the MusiXmatch dataset provided
by \href{http://labrosa.ee.columbia.edu/millionsong/musixmatch}{Columbia
University's Million Song Dataset}. This dataset provided us with a total of
237,662 music tracks. However, we found that the dataset was lacking in
information and missing a lot of data. To resolve our dataset problem, we
had to engineer a new strategy.\\
\\
After some investigation, we found that the music tracks provided by the
Million Song Dataset had track-id's that could be used with the MusiXmatch
API. We found a Python API Wrapper that would make queries to MusiXmatch,
however, it was implemented with Python 2.7. To solve any compatibility
issues, we re-wrote the Wrapper for Python 3.5. While re-writing it, we
improved some of the functions and customized some aspects for our purposes.
Through the use of a Python API Wrapper, the API provided us with a lot of
metadata about different tracks, including a bag-of-words representation of
the lyrics.\\
\\
Unfortunately, the bag-of-words that was provided by the API were preprocessed
and stemmed. This meant that words were cut down to their root and all
contractions were disassembled. In music, we believe that it is not just the
words that are used, but how they are used that helps determine genre. Thus,
we decided (rather than using the bag-of-words provided) we would create a web
scraper that would grab the lyrics from a url provided by the API. We verified
that the words found by this URL are the unprocessed words that were found in
the bag-of-words. This gave us the ability to process the lyrics as we wished
and create our own bag-of-words.\\
\\
We created a script which would iterate through all of the 237,662 tracks
provided, make an API call for each track, determine whether the track was in
English, and if so gather the information we wanted (this includes the web
scraping). It then stored the data into appropriately labeled files (described
later). Unfortunately, after leaving the program running overnight (since the
process was extremely time consuming) we discovered that the API keys we
created had a limited number of API calls for free users. It had cut off at
only 5,000 songs, far from our total track id data of 237,662 tracks.
Ultimately, we created various accounts giving us different API keys to use.
We then updated our program to swap API keys once one had expired.
Unfortunately, the process was still very time consuming.\\
\\
After running the program more and more, we discovered that there are problems
with some of the id's provided. This included tracks that were not in English,
faulty track id's, tracks that did not have lyrics (instrumental songs),
tracks that had no genre, and tracks that had multiple genres. Finally, there
were some time-out issues (while accessing the API or retrieving lyrics) that
would occur from time to time.\\
\\
To solve all of these problems, we filled our program with try's and except's
to catch these errors and created several different files to organize the
data. For the tracks that were not in English, we would write their track id
to $no\_en.txt$. For non-functioning id's, we wrote their track id to
$error.txt$. For instrumental tracks, we wrote their track id to
$no\_lyrics.txt$. For tracks that did not have a genre, but had the rest of
the information we wanted, we wrote the information to $find\_genre.json$. For
tracks with multiple genres and all other information we seek, we wrote the
information to $narrow\_genre.json$. The tracks that contained all of the data
we desired (shown in Figure 1.a) and only had one genre were written to
$data.json$. Finally, for time-outs, we wrote the track id to $redo.txt$. We
had planned on running the program again using these redo id's to collect more
data, however, there was not enough time.\\
\\
To summarize this process, we used the dataset provided by
\href{http://labrosa.ee.columbia.edu/millionsong/musixmatch}{MusiXmatch}, for
the track id's. From there, we have began to construct a more fulfilled
dataset through API calls to MusiXmatch and web scraping to gather complete
lyrics. This entire process is illustrated in Figure 1.b.

\begin{figure}[ht]
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/song_data.png}
        \caption{Sample of Song Data Collected}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{pics/data_process.png}
        \caption{Data Compilation Process}
    \end{subfigure}
    \caption{Data Process}
\end{figure}\\

Due to the slow nature of web crawling and API queries we were only able to
gather over 20,000 tracks from the 237,662 tracks provided by the
\textit{Million Song Dataset}. Of the tracks we gathered, 3,471 of the songs
do not have a genre associated to them, 8,497 has multiple genres associated
to them, and 19,480 had only one genre associated to them. The statistics for
the 55,110 tracks that we did iterate over, are as follows:\\
\\
\begin{tabular}{|c|c|c|c|c|c|c|}
    \hline
    Non-English & Errors & Instrumental & Redo &
    No Genre & Multiple Genres & Main Data
    \\ \hline
    6,797 & 5,975 & 312 & 10,547 & 3,471 & 8,497 & 19,480
    \\ \hline
\end{tabular}\\
\\

Additionally, once we had our data we discovered taht there were a total of 83
different genres. To reduced this, we created a genre mapping that converted
subgenres to parent genres (i.e. Soft Rock and Indie Rock became Rock). After
accomplising this goal, we ended up with a total of 10 genres. The figures
bellow demonstrate this mapping. Figure 2.a is a rough approximation to how
genres were mapped while Figure 2.b shows our main data.

\begin{figure}[ht]
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth, height=0.2\textheight]{pics/all_genres.png}
        \caption{Genre Mapping Mechanism}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/data_pie.png}
        \caption{Main Data}
    \end{subfigure}
    \caption{Genre Mappings and Data}
\end{figure}


\section{Description of Technical Approach}

After seeing some of the tracks in our dataset, we brainstormed various
improvements we could make while processing the lyrics. We looked back at the
original dataset we began with to make decisions for our new dataset. The
original dataset provided a default bag-of-words for 237,662 different music
tracks. that were preprocessed using stemming (e.g. frustrate or frustrating
became frustrat). The default data also removed all punctuation including
apostrophes. We believe that these removals took away from contextual
information since each word in a song is picked in a meaningful manner (e.g.
hanging might be more likely to appear in a pop song, while hangin' might be
more likely to appear in a rap song).\\
\\
Our primary language used was Python. We also used API's to collect our data
(described in Section 2), the NLTK library for all text filtering and the
creation of our bag-of-words, Scikit-Learn for our classification algorithms,
and matplotlib for data visualization.  Our aim was to create a pipeline that
would be fed the data and classifier, then interpret our results. A figure of
this pipeline is illustrated bellow.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.4\textwidth, height=0.1\textheight]{pics/pipeline.png}
  \caption{Pipeline Illustration of the Code we will write}
\end{figure}

\subsection{Classifying Algorithms}

We used several different classification algorithms to conduct our
experiments. Some of these classifiers were ones that we had used before and
decided on them due to comfort. Others, however, we had never heard of before.
Thus, we did our research and selected the best classifiers we could.

\subsubsection{Multinomial Naive Bayes}
Multinomial Bayes was our safety net since we had already worked with the
classifier before. Bayes algorithms consider each value independent from every
other value. Each data point, or song word, will contribute independently to the
probability of the song genre. Given the words of a song, a weight is given to
each individual word and a resulting value is calculated to predict the genre.

\subsubsection{K-Nearest Neighbors}
K-Nearest Neighbors is a way of predicting a classification based off
of similar groupings. Songs would be plotted to different points based on their
genre score. To classify a new song, you would then compute which genre has the
K-nearest neighbors to our new song. Thus new songs are based on where they
appear on the plot and their surrounding neighbors.

\subsubsection{Decition Trees}
Decision Trees would try to sort things by whether or not certain words could
break things into different categories. For instance there are 1000 documents
with 90\% of those documents belonging to one category and 10\% belonging to another.
A maximum gain would be if a word always appeared in the 10\% set, but never in
the other. That could perfectly split the original group into 100/100 ``word"
appears, and 0/900 ``word never appears".

\subsubsection{One vs. Rest (using Linear SVC)}
One versus the Rest is more of a wrapper than a classifier. It works by
transforming a multi-classification problem into a binary classification
problem. To do this, if focuses on one label at a time and gives it a score of
$+1$, all other labels then become $-1$. It then calculates values for each
label, finally returning the label with the highest score. Thus, if we have 5
labels, it will compute 5 different scores and return the maximum. To
accomplish this transformation, the wrapper takes in a binomial classifier (in
this case we used Linear SVC).

\subsubsection{Linear SVC}
Linear SVC works by binerizing values (i.e. belonging to one set or another).
As it builds the model, it assigns examples to one set or another making it
non-probabilistic. The model itself is a representation of the examples as
points in space separated by a line. It differs from traditional SVC as it has
more flexibility in its choice of penalties and loss functions.


\section{Software}

\subsection{Code Written}

There were several differnt phases to the project. Thus, we have divided the
code we wrote into their appropriate categories.

\subsubsection{Data Collection}

We wrote a script to extract all MusiXmatch song id's that would later be used
for API queries. We then wrote an API wrapper to make the queries to
MusiXmatch. Finally, we created a pipeline to collect all of our data. It uses
the MusiXmatch API to gather metadata and scrapes the MusiXmatch website for
lyrics. This is done by using the track ID information provided in the
original dataset. This code then sorts the data into either a $json$ file that
has genre information, a $json$ file that has no genre information, a $json$
file that has multiple genres, and a few other files.

\subsubsection{Code/Scripts Successfully Used on Data}

We wrote a script that takes the complete lyrics found in our $json$ files for
each song and converts them into a bag-of-words. We did this by using the NLTK
library provided by the Anaconda version of Python and using the pre-built
functions to tokenize the lyrics, remove the stopwords, and get a bag of
words.\\
\\
We also created a script that would set up our data (process it) and feed it
to our classifiers. This script would load the data from a given file and
extract the features we needed.\\
\\
Finally, we created a pipeline for classification. This included the
classification algorithms: Multinomial Bayes, K-Nearest Neighbors, Decision
Trees, and One v Rest (Linear SVC). These functions would be fed the processed
data and spit out our results.

\subsection{Code From Other Sources}

We used three different open source projects as outside sources. These
included NLTK, scikit-learn, and matplotlib.

\subsubsection{NLTK}

We used this primarly to help us create bag-of-word representation of our
data. This was useful in creating frequency distributions of our genres and
comparing word usage accross genres.

\subsubsection{scikit-learn}

This was used primarily for classifier algorithms. We were able to get all of
the classifiers we used and import them from this library to our scripts. In
addition, we used this library for data processing and evaluation (confusion
matrix).

\subsubsection{matplotlib}

This library was used solely for data visualization. These visualizations were
used for both evaluation and for creating this report.



\section{Experiments and Evaluation}

We conducted various different experiments and reached varying results. For
all of our experiments, we divided our data into an 80/20 split. This meant
that 80 percent of our data was to be used for training and 20 for testing.

\subsection{Using All Data}

Our first (and very naive) attempt was done after completing our
classification pipeline. We simply loaded our data, processed it, then fed
each classifier the same training and test datasets. Our results were
devastating.\\
\begin{figure}[ht]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        Classifier
        &
        Accuracy
        &
        Increase from Baseline
        \\ \hline
        Majority Classifier & $56.6\%$ &
        \\ \hline
        Multinomial Naive Bayes & $56.5\%$ & $-0.1\%$
        \\ \hline
        k Nearest Neighbors (k=50, w='distance`) & $58\%$ & $+1.4\%$
        \\ \hline
        Decision Trees & $47.31\%$ & $-9.29\%$
        \\ \hline
        One vs. Rest (Linear SVC) & $60.36\%$ & $+3.76\%$
        \\ \hline
    \end{tabular}
        \caption{Naive First Run}
\end{figure}\\
\\
These results were truly devastating. After all of our hard work collecting
our data set and brainstorming ideas for improving classification, the best we
did was a $4\%$ increase from our baseline.

\subsection{Narrowing Down Genres}

We were determined to figure out why we did so poorly and thus we dove into
our data. We decided to take a look at the vocabulary that was used for each
genre, and the frequency of words. To do this, we created histograms of genre
vocabulary. Below are histograms of the 25 most common words in the Country,
Rock, and Hip-Hop/Rap genres (be warned, these are not censored).

\begin{figure}[ht]
    \begin{subfigure}{0.32\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/country_hist.png}
        \caption{Country}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/rock_hist.png}
        \caption{Rock}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.32\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/hip_hist.png}
        \caption{Hip-Hop/Rap}
    \end{subfigure}
    \caption{Top 25 Histogram}
\end{figure}\\
We did this for all 10 genres and we found that only in the top 25 words for
each genre, there was an overlap of 9 words. This means that $36\%$ of the top
25 words in a genre is universal across all genres.\\
\\

This lead us to investigate more, and we found that from the top 50 words
there is an intersection of 22 words. From the top 100 words, there is an
intersection of 37 words. Due to high intersection, we decided to discover
what percentage of vocabulary is shared amongst genres. We discovered that
$68\%$ of each genres vocabulary was shared.\\
\\
After this discovery, we took a step back and decided to approach the problem
in a different manner. Rather than compare all genres at once, we compared
only 2-3 genres at a time. This lead to much more satisfactory results.

\begin{figure}[ht]
    \begin{subfigure}{0.9\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            Classifier
            &
            Accuracy
            &
            Increase from Baseline
            \\ \hline
            Majority Classifier & $67.1\%$ &
            \\ \hline
            Multinomial Naive Bayes & $82.9\%$ & $+15.8\%$
            \\ \hline
            k Nearest Neighbors (k=100, w='distance`) & $89.5\%$ & $+22.4\%$
            \\ \hline
            Decision Trees & $88.9\%$ & $+21.8\%$
            \\ \hline
            One vs. Rest (Linear SVC) & $92.1\%$ & $+25\%$
            \\ \hline
        \end{tabular}
        \caption{Country vs. Hip-Hop/Rap}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.9\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            Classifier
            &
            Accuracy
            &
            Increase from Baseline
            \\ \hline
            Majority Classifier & $94.5\%$ &
            \\ \hline
            Multinomial Naive Bayes & $94.5\%$ & $0\%$
            \\ \hline
            k Nearest Neighbors (k=25, w='distance`) & $96.3\%$ & $+1.8\%$
            \\ \hline
            Decision Trees & $96.7\%$ & $+2.2\%$
            \\ \hline
            One vs. Rest (Linear SVC) & $97.4\%$ & $+2.9\%$
            \\ \hline
        \end{tabular}
        \caption{Hip-Hop/Rap vs. Rock}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.9\textwidth}
        \centering
        \begin{tabular}{|c|c|c|}
            \hline
            Classifier
            &
            Accuracy
            &
            Increase from Baseline
            \\ \hline
            Majority Classifier & $71.2\%$ &
            \\ \hline
            Multinomial Naive Bayes & $72.1\%$ & $+0.9\%$
            \\ \hline
            k Nearest Neighbors (k=10, w='uniform`) & $75.8\%$ & $+4.6\%$
            \\ \hline
            Decision Trees & $71.2\%$ & $0\%$
            \\ \hline
            One vs. Rest (Linear SVC) & $84.1\%$ & $+12.9\%$
            \\ \hline
        \end{tabular}
        \caption{Pop vs. Metal vs. Christian Gospel}
    \end{subfigure}
    \caption{Comparing Specific Genres}
\end{figure}

With this, we were able to see that certain genres were clearly
distinguishable (Country vs. Hip-Hop/Rap). In addition, we noticed that when
classifying more than 2 genres, our scores began to decay again. However, the
Hip-Hop/Rap vs. Rock match up bugged us. The data was primarily dominated by
Rock (94.5\%), signifying that there really wasn't much room for improvement.
So, we decided to even out the playing field.

\subsection{Evening Out Genre Fields}

Since we saw that Rock had this clear dominance in our data, we decided adapt.
Rather than do an 80/20 split on each genre individually, we would do an 80/20
split on the genre with the least amount of tracks. As an example, Rock had
10764 songs while Country had 1012. Thus, rather than train on 8611 Rock and
809 Country, we would train on 809 of both. In addition, instead of testing on
2153 Rock and 203 Country, we would test on only 203 for both.

\subsubsection{All Genres}

With this new implementation, we decided to see how much our classifier would
increase using all 10 genres.

\begin{figure}[ht]
    \centering
    \begin{tabular}{|c|c|c|}
        \hline
        Classifier
        &
        Accuracy
        &
        Increase from Baseline
        \\ \hline
        Majority Classifier & $10\%$ &
        \\ \hline
        Multinomial Naive Bayes & $41.18\%$ & $+31.18\%$
        \\ \hline
        k Nearest Neighbors (k=100, w='distance`) & $36\%$ & $+26\%$
        \\ \hline
        Decision Trees & $28.59\%$ & $+18.59\%$
        \\ \hline
        One vs. Rest (Linear SVC) & $40.59\%$ & $+30.59\%$
        \\ \hline
    \end{tabular}
        \caption{Even Genre Run}
\end{figure}

These results were much more satisfying as we saw an average increase of
26.59\% across classifiers. We then dove into the confusion matrices to see
where the classifiers were going wrong.

\begin{figure}[ht]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/cmmnb.png}
        \caption{Multinomial NB}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.48\textwidth}
        \centering
        \includegraphics[width=0.9\textwidth]{pics/cmovr.png}
        \caption{One vs. Rest (Linear SVC)}
    \end{subfigure}
    \caption{Confusion Matrix}
\end{figure}

As we can see, Hip-Hop/Rap is being classified correctly most of the time.
However, there are inconsistency across the board. There are other genres such
as Christian & Gospel, Country, Metal, Pop, and Rock which are being
classified fairly correctly. Unfortunately, Electronic and Other are
classified horribly.


\subsubsection{Pairs of Genres}

We also saw an increase in specific genre classification. Classification for
Hip-Hop/Rap and Country increased from 25\% to 41.2\%. Hip-Hop/Rap versus Rock
increased from 2.9\% to 39.2\%.\\

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth, height=0.2\textheight]{pics/specific.png}
    \caption{Even Genre Run With Specific Genres}
\end{figure}

Although we did see significant increases after we changed our approach, we
were still not satisfied with our results. We concluded that there exists a
cap on how well a classifier can do. This is due to the intersecting
vocabulary of the genres. However, we are satisfied with the results when
comparing paired genres. By leveling the playing field, we saw drastically
better results.

\section{Discussion / Conclusion}

The results that agreed with our expectations were that contrasting song
genres, like Christian & Gospel vs. Hip-Hop & Rap, had the highest sorting
accuracy(~90\%) while more similar genres like Country vs. Rock had a worse
sorting accuracy (~50\% and 75\%). What surprised us was how high our accuracy
was in some of the results.\\
\\
One major limitation of our project was the data gathering aspect. While the
original papers had datasets of around 150,000, in the time we had our web
scraper was only able to gather 19,015 songs. This lack of data might have
explained why our results were higher than in previous papers done on the
subject. A future limitation of this project would be that since genre
classification is so subjective even a human would have trouble with it, our
classifiers will never be foolproof. That is to say our classifiers will
likely never have the ability to run on lyrics and return the correct answer
often enough to be of much use in a practical sense.\\
\\
If we were in charge of a research lab, the first thing we would do is have
human trials so that we have a better baseline to compare our results to.
While our classifier does better than randomly sorting in almost all cases, if
it well below the level of what a human could do, there would be far fewer
practical applications for our methods. Additionally we would want to remove
the intersection of the top k words from each genre, since about 50-70\% of
most words overlap between song genres. Finally, as a curiosity we would like
to apply our classifiers to poetry in order to see what genres famous poems
are most similar to.



\end{document}
