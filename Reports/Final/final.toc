\contentsline {section}{\numberline {1}Background / Description}{2}{section.1}
\contentsline {section}{\numberline {2}Related Work}{2}{section.2}
\contentsline {section}{\numberline {3}Data Sets}{2}{section.3}
\contentsline {section}{\numberline {4}Description of Technical Approach}{4}{section.4}
\contentsline {subsection}{\numberline {\textbf {4.a}}Classifying Algorithms}{5}{subsection.4.1}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.1}Multinomial Naive Bayes}{5}{subsubsection.4.1.1}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.2}K-Nearest Neighbors}{5}{subsubsection.4.1.2}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.3}Decition Trees}{5}{subsubsection.4.1.3}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.4}One vs. Rest (using Linear SVC)}{5}{subsubsection.4.1.4}
\contentsline {subsubsection}{\numberline {\textbf {4.a}.5}Linear SVC}{5}{subsubsection.4.1.5}
\contentsline {section}{\numberline {5}Software}{6}{section.5}
\contentsline {subsection}{\numberline {\textbf {5.a}}Code Written}{6}{subsection.5.1}
\contentsline {subsubsection}{\numberline {\textbf {5.a}.1}Data Collection}{6}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {\textbf {5.a}.2}Code/Scripts Successfully Used on Data}{6}{subsubsection.5.1.2}
\contentsline {subsection}{\numberline {\textbf {5.b}}Code From Other Sources}{6}{subsection.5.2}
\contentsline {subsubsection}{\numberline {\textbf {5.b}.1}NLTK}{6}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {\textbf {5.b}.2}scikit-learn}{6}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {\textbf {5.b}.3}matplotlib}{7}{subsubsection.5.2.3}
\contentsline {section}{\numberline {6}Experiments and Evaluation}{7}{section.6}
\contentsline {subsection}{\numberline {\textbf {6.a}}Using All Data}{7}{subsection.6.1}
\contentsline {subsection}{\numberline {\textbf {6.b}}Narrowing Down Genres}{7}{subsection.6.2}
\contentsline {subsection}{\numberline {\textbf {6.c}}Evening Out Genre Fields}{8}{subsection.6.3}
\contentsline {subsubsection}{\numberline {\textbf {6.c}.1}All Genres}{9}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {\textbf {6.c}.2}Pairs of Genres}{9}{subsubsection.6.3.2}
\contentsline {section}{\numberline {7}Discussion / Conclusion}{10}{section.7}
