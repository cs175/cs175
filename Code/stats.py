#!/usr/bin/env python3

""".

stats.py

    Creator:
        Author  : Heliodoro Tejeda
        E-mail  : htejeda.uci@gmail.com
        Date    : 7 Mar. 2016

Simple stats script to gather and visualize data.
"""

import lyrics_to_bow as bow
import matplotlib.pyplot as plt
import numpy as np
import json


def load_data(fpath):
    """
    Load the music dataset.

    Parameters
    ----------
    fpath   : str
        File path to the data file we want to open. Must be a json file.

    Returns
    -------
    Dict
        The dataset in dictionary form.
    """
    with open(fpath) as f:
        data = json.load(f)
    return data


def most_common_k_words_per_genre(genre, k=25):
    """
    Create a histogram for the most common k words in a given genre.

    Returns nothing but plots the figure.

    Parameters
    ----------
    genre   : str
        A music gener to analyze

    k       : int
        An interger for the k most common words to graph

    Returns
    -------
    None
    """
    data = load_data('data/train.json')
    genre_dist = None
    for track in data[genre]:
        if not genre_dist:
            genre_dist = bow.bow_pipeline(data[genre][track]['lyrics'])
        else:
            genre_dist += bow.bow_pipeline(data[genre][track]['lyrics'])
    most_common = genre_dist.most_common(k)
    words, count = [], []
    for w, c in most_common:
        words.append(w), count.append(c)
    indexes, width = np.arange(len(words)), 0.5
    plt.bar(indexes, count, width)
    plt.xticks(indexes + width * 0.5, words, rotation='vertical')
    plt.show()
    return None


def determine_common_word_overlap(k=25):
    """
    Determine the overlap of the k most common words for all genres.

    Returns an intersection of words.

    Parameters
    ----------
    k   : int
        Amount of words we want

    Returns
    -------
    List
        Intersection of words from all genres
    """
    data = load_data('data/train.json')
    genre_freq_list = []
    genres_list = []
    for genre in data.keys():
        genres_list.append(genre)
        freq_list = None
        for track in data[genre]:
            if not freq_list:
                freq_list = bow.bow_pipeline(data[genre][track]['lyrics'])
            else:
                freq_list += bow.bow_pipeline(data[genre][track]['lyrics'])
        genre_freq_list.append(freq_list)
    words = None
    print(genres_list)
    print()
    for dist in genre_freq_list:
        print(words)
        if not words:
            words = set([t[0] for t in dist.most_common(k)])
        else:
            dist_set = set([t[0] for t in dist.most_common(k)])
            print(dist_set)
            words = words & dist_set
        print()
    return words


def tracks_per_genre():
    """
    Determine the amount of tracks in each genre.

    Parameters
    ----------

    Returns
    -------
    [Tuple]
        List of tuples containing genre name and track counts
    """
    data = load_data('data/train.json')
    info = []
    for genre in data.keys():
        info.append((genre, len(data[genre].keys())))
    return info



if __name__ == '__main__':
    most_common_k_words_per_genre('Hip-Hop/Rap')
    #w = determine_common_word_overlap(k=100)
    #print(w)
    #info = tracks_per_genre()
#    #for i in info:
#        print(i)
