#!/usr/bin/env python3

""".

main.py

    Creator:
        Name    : Heliodoro Tejeda
        E-mail  : htejeda.uci@gmail.com
        Date    : 4 Mar. 2016

This will be the main script run for our project. It will host the main
    pipeline we will use for our classification.
"""

import setup_data as setup
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix


def extract_text_features(train_data, test_data):
    """
    Return two types of training and test data features.

        1)  Bags of words (BOWs): X_train_counts, X_test_counts
        2)  Term Frequency times Inverse Document Frequency (tf-idf):
                X_train_tfidf, X_test_tfidf

    How to create BOW features:
        You need to first generate a count vector from the input data,
        excluding the NLTK stopwords. This can be done by importing the
        English stopword list from NLTK and then passing it to a
        CountVectorizer during initialization.

        CountVectorizer is slightly different than the FreqDist object you
        used in your previous assignment.  Where FreqDist is good at creating
        a dict-like bag-of-words representation for a single document,
        CountVectorizer is optimized for creating a sparse matrix representing
        the bag-of-words counts for every document in a corpus of documents
        all at once.  Both objects are useful at different times.

    How to create tf-idf features:
        tf-idf features can be computed using TfidfTransformer with the count
        matrix (BOWs matrix) as an input. The fit method is used to fit a
        tf-idf estimator to the data, and the transform method is used
        afterwards to transform either the training or test count-matrix to a
        tf-idf representation. The method fit_transform strings these two
        methods together into one.

        For the training data, you'll want to use the fit_transform method to
        both fit the tf-idf model and then transform the training count matrix
        into a tf-idf representation.

        For the test data, you only need to call the transform method since
        the tf-idf model will have already been fit on your training set.

    Parameters
    ----------
    train_data : List[str]
        Training News data in list

    test_data : List[str]
        Test data in list

    Returns
    -------
    Tuple(scipy.sparse.csr.csr_matrix,..)
        Returns X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf
            as a tuple.
    """
    # Tokenize the text and store the result in 'X_train_counts'
    from sklearn.feature_extraction.text import CountVectorizer
    from nltk.corpus import stopwords

    # YOUR SOLUTION STARTS HERE###
    from sklearn.feature_extraction.text import TfidfTransformer
    tfidf_transformer = TfidfTransformer()
    X_set_count = CountVectorizer(stop_words=stopwords.words('english'))
    # Training Data
    X_train_counts = X_set_count.fit_transform(train_data)
    tfidf_transformer = tfidf_transformer.fit(X_train_counts)
    X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
    # Test Data
    X_test_counts = X_set_count.transform(test_data)
    X_test_tfidf = tfidf_transformer.transform(X_test_counts)
    return X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf


def fit_and_predict_multinomialNB(X_train, Y_train, X_test):
    """
    Train multinomial naive Bayes model.

    With 'X_train' and 'Y_train' and predict the Y values for 'X_test' (Use
    'MultinomialNB' from scikit-learn). Return the predicted Y values.

    Parameters
    ----------
    X_train: scipy sparse matrix
        Data for training (matrix with features, e.g. BOW or tf-idf)
    Y_train: numpy.ndarray
        Labels for training data (target value)
    X_test: scipy sparse matrix
        Test data used for prediction

    Returns
    -------
    numpy.ndarray[int]
        Target values predicted from 'X_test'
    """
    # Import the package
    from sklearn.naive_bayes import MultinomialNB

    # YOUR SOLUTION STARTS HERE###
    clf = MultinomialNB().fit(X_train, Y_train)
    predicted_multNB = clf.predict(X_test)
    return predicted_multNB


def fit_and_predict_knn(X_train, Y_train, X_test, neighbors, weight):
    """
    Train k-nearest neighbors model.

    With 'X_train' and 'Y_train' and predict the Y values for 'X_test'. (Use
    'KNeighborsClassifier' from scikit-learn). Return the predicted Y values.

    Parameters
    ----------
    X_train: scipy sparse matrix
        Data for training (matrix with features, e.g. BOW or tf-idf)
    Y_train: numpy.ndarray
        Labels for training data (target value)
    X_test: scipy sparse matrix
        Test data used for prediction
    neighbors: int
        The number of neightbors to use for the algorithm
    wieght: str
        Either 'uniform' or 'distance' to be used with algorithm

    Returns
    -------
    numpy.ndarray[int]
        Target values predicted from 'X_test'
    """
    # Import the package
    from sklearn.neighbors import KNeighborsClassifier

    # YOUR SOLUTION STARTS HERE###
    clf = KNeighborsClassifier(n_neighbors=neighbors, weights=weight)
    clf.fit(X_train, Y_train)
    predicted_knn = clf.predict(X_test)
    return predicted_knn


def fit_and_predict_dtree(X_train, Y_train, X_test):
    """
    Train Decision Tree model.

    With 'X_train' and 'Y_train' and predict the Y values for 'X_test'. (Use
    'DecisionTreeClassifier' from scikit-learn). Return the predicted Y
    values.

    Parameters
    ----------
    X_train: scipy sparse matrix
        Data for training (matrix with features, e.g. BOW or tf-idf)
    Y_train: numpy.ndarray
        Labels for training data (target value)
    X_test: scipy sparse matrix
        Test data used for prediction

    Returns
    -------
    numpy.ndarray[int]
        Target values predicted from 'X_test'
    """
    # Import the package
    from sklearn.tree import DecisionTreeClassifier

    # YOUR SOLUTION STARTS HERE###
    clf = DecisionTreeClassifier().fit(X_train, Y_train)
    predicted_dtree = clf.predict(X_test)
    return predicted_dtree


def fit_and_predict_ovr(X_train, Y_train, X_test):
    """
    Train One vs. Rest model.

    With 'X_train' and 'Y_train' and predict the Y values for 'X_test'. (Use
    'OneVsRestClassifier' from scikit-learn). Return the predicted Y values.

    Parameters
    ----------
    X_train: scipy sparse matrix
        Data for training (matrix with features, e.g. BOW or tf-idf)
    Y_train: numpy.ndarray
        Labels for training data (target value)
    X_test: scipy sparse matrix
        Test data used for prediction

    Returns
    -------
    numpy.ndarray[int]
        Target values predicted from 'X_test'
    """
    # Import the package
    from sklearn.multiclass import OneVsRestClassifier
    from sklearn.svm import LinearSVC
    # YOUR SOLUTION STARTS HERE###
    clf = OneVsRestClassifier(LinearSVC(random_state=0)).fit(X_train, Y_train)
    predicted_ovr = clf.predict(X_test)
    return predicted_ovr


def fit_and_predict_ovo(X_train, Y_train, X_test):
    """
    Train One vs. One model.

    With 'X_train' and 'Y_train' and predict the Y values for 'X_test'. (Use
    'OneVsOneClassifier' from scikit-learn). Return the predicted Y values.

    Parameters
    ----------
    X_train: scipy sparse matrix
        Data for training (matrix with features, e.g. BOW or tf-idf)
    Y_train: numpy.ndarray
        Labels for training data (target value)
    X_test: scipy sparse matrix
        Test data used for prediction

    Returns
    -------
    numpy.ndarray[int]
        Target values predicted from 'X_test'
    """
    # Import the package
    from sklearn.multiclass import OneVsOneClassifier
    from sklearn.svm import LinearSVC
    # YOUR SOLUTION STARTS HERE###
    clf = OneVsOneClassifier(LinearSVC(random_state=0)).fit(X_train, Y_train)
    predicted_ovo = clf.predict(X_test)
    return predicted_ovo


def single_run_even(labels=['Christian & Gospel', 'Country', 'Electronic',
                            'Hip-Hop/Rap', 'Jazz & Blues', 'Metal', 'Pop',
                            'RnB/Soul', 'Rock', 'Other'],
                    print_stats=True, print_accuracies=True):
    """
    Do a single run using even track amounts.

    This uses all classifiers and prints results. These values can be
    modified.

    Parameters
    ----------
    labels  : List
        By default includes all 10 genres of music. can be modified to include
        a subset of genres. NOTE: there must be at least 2 genres.

    print_stats : Boolean
        Determines whether or not to print statistics such as:
            Dimensions of X_train_counts, Non-zero elements,
            Avg number of words/document, Avg number of doc/word
        By default True

    print_accuracies    : Boolean
        Determines whether or not to print the accuracy of a classifier as
        its process finishes.
        By default True.

    Returns
    -------
    List [(Accuracies, Predictions)]
        List of tuple values.
            (Accuracies, # --> accuracy of classifier
             Predictions # --> predicted values from classifier)
    """
    train, test = setup.set_up_even_genres('data/train.json', labels)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf =\
        extract_text_features(train['data'], test['data'])
    if (print_stats):
        num_non_zero = X_train_counts.nnz
        av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
        av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
        print()
        print('Dimensions of X_train_counts are', X_train_counts.shape)
        print()
        print('Number of non-zero elements in X_train_counts:', num_non_zero)
        print('Average number of word tokens per document:',
              "%.4f" % av_num_word_tokens_per_doc)
        print('Average number of documents per word token:',
              "%.4f" % av_num_docs_per_word_token)
        print()
    # NOTE:
    #   There is no value checking done for labels. We assume that proper
    #   labels will be used. This means no redundant labels and that the
    #   labels list is not empty.
    #
    #       MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)
    #
    #       KNN
    k_w = [(5, 'uniform'), (10, 'uniform'), (25, 'uniform'), (50, 'uniform'),
           (100, 'uniform'), (250, 'uniform'),
           (5, 'distance'), (10, 'distance'), (25, 'distance'),
           (50, 'distance'), (100, 'distance'), (250, 'distance')]
    predicted_knn_list = []
    for i, j in k_w:
            predicted_knn = fit_and_predict_knn(
                                X_train_tfidf, train_targets, X_test_tfidf,
                                i, j)
            predicted_knn_list.append(predicted_knn)
    #
    #       Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    #
    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    if (print_accuracies):
        # MultinomialNB
        print('Accuracy with multinomial naive Bayes:',
              '%.4f' % np.mean(predicted_multNB == test_targets))
        # KNN
        print('Accuracy with k-nearest neighbors')
        for i, j in enumerate(k_w):
            print('\tk={}, w={}: '.format(j[0], j[1]),
                  '%.4f' % np.mean(predicted_knn_list[i] == test_targets))
        # Decision Tree
        print('Accuracy with Decision Tree: ',
              '%.4f' % np.mean(predicted_dtree == test_targets))
        # One v. Rest
        print('Accuracy with One v Rest: ',
              '%.4f' % np.mean(predicted_ovr == test_targets))
    return (test_targets, predicted_multNB, predicted_knn_list,
            predicted_dtree, predicted_ovr)


def single_run_all(labels=['Christian & Gospel', 'Country', 'Electronic',
                           'Hip-Hop/Rap', 'Jazz & Blues', 'Metal', 'Pop',
                           'RnB/Soul', 'Rock', 'Other'],
                   print_stats=True, print_accuracies=True):
    """
    Do a single run using all tracks.

    This uses all classifiers and prints results. These values can be
    modified.

    Parameters
    ----------
    labels  : List
        By default includes all 10 genres of music. can be modified to include
        a subset of genres. NOTE: there must be at least 2 genres.

    print_stats : Boolean
        Determines whether or not to print statistics such as:
            Dimensions of X_train_counts, Non-zero elements,
            Avg number of words/document, Avg number of doc/word
        By default True

    print_accuracies    : Boolean
        Determines whether or not to print the accuracy of a classifier as
        its process finishes.
        By default True.

    Returns
    -------
    List [(Accuracies, Predictions)]
        List of tuple values.
            (Accuracies, # --> accuracy of classifier
             Predictions # --> predicted values from classifier)
    """
    train, test = setup.set_up_test_on_training('data/train.json')
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf =\
        extract_text_features(train['data'], test['data'])
    if (print_stats):
        num_non_zero = X_train_counts.nnz
        av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
        av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
        print()
        print('Dimensions of X_train_counts are', X_train_counts.shape)
        print()
        print('Number of non-zero elements in X_train_counts:', num_non_zero)
        print('Average number of word tokens per document:',
              "%.4f" % av_num_word_tokens_per_doc)
        print('Average number of documents per word token:',
              "%.4f" % av_num_docs_per_word_token)
        print()
    # NOTE:
    #   There is no value checking done for labels. We assume that proper
    #   labels will be used. This means no redundant labels and that the
    #   labels list is not empty.
    #
    #       MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)
    #
    #       KNN
    k_w = [(5, 'uniform'), (10, 'uniform'), (25, 'uniform'), (50, 'uniform'),
           (100, 'uniform'), (250, 'uniform'),
           (5, 'distance'), (10, 'distance'), (25, 'distance'),
           (50, 'distance'), (100, 'distance'), (250, 'distance')]
    predicted_knn_list = []
    for i, j in k_w:
            predicted_knn = fit_and_predict_knn(
                                X_train_tfidf, train_targets, X_test_tfidf,
                                i, j)
            predicted_knn_list.append(predicted_knn)
    #
    #       Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    #
    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    if (print_accuracies):
        # MultinomialNB
        print('Accuracy with multinomial naive Bayes:',
              '%.4f' % np.mean(predicted_multNB == test_targets))
        # KNN
        print('Accuracy with k-nearest neighbors')
        for i, j in enumerate(k_w):
            print('\tk={}, w={}: '.format(j[0], j[1]),
                  '%.4f' % np.mean(predicted_knn_list[i] == test_targets))
        # Decision Tree
        print('Accuracy with Decision Tree: ',
              '%.4f' % np.mean(predicted_dtree == test_targets))
        # One v. Rest
        print('Accuracy with One v Rest: ',
              '%.4f' % np.mean(predicted_ovr == test_targets))
    return (predicted_multNB, predicted_knn_list, predicted_dtree,
            predicted_ovr)


def cross_validation(k=5, even=True):
    """
    Use cross validation to get an average of accuracies returned.

    Parameters
    ----------
    k   : int
        Amount of times to run predictors to get average of accuracies.

    even    : Boolean
        By default True, this indacates that single_run_even will be used.
        If set to False, then single_run_all will be used.

    Returns
    -------
    List[Averages]
        List of averages of each classifier
    """
    classifier_accuracies = []
    if (even):
        for i in range(k):
            classifier_accuracies.append(single_run_even())
    else:
        for i in range(k):
            classifier_accuracies.append(single_run_all())
    mltnb, knn, dtree, ovr = 0, 0, 0, 0
    for i in range(k):
        mltnb += classifier_accuracies[i][0]
        knn += classifier_accuracies[i][1]
        dtree += classifier_accuracies[i][2]
        ovr += classifier_accuracies[i][3]
    mltnb /= 5
    knn /= 5
    dtree /= 5
    ovr /= 5
    return mltnb, knn, dtree, ovr


def plot_confusion_matrix(cm, title, labels, cmap=plt.cm.YlOrRd):
    """
    Plot the confusion matrix given.

    Parameters
    ----------
    cm  : Confusion Matrix

    title   : string
        Title to give to plot

    cmap    : plt.cm
        Color map for plot

    labels  : list
        List of genre labels associated with cm

    Returns
    -------
    None
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title('Confusion Matrix: {}'.format(title))
    plt.colorbar()
    tick_marks = np.arange(len(labels))
    plt.xticks(tick_marks, labels, rotation='vertical')
    plt.yticks(tick_marks, labels)
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


def get_confusion_matrix(predicted,
                         labels=['Christian & Gospel', 'Country',
                                 'Electronic', 'Hip-Hop/Rap', 'Jazz & Blues',
                                 'Metal', 'Pop', 'RnB/Soul', 'Rock', 'Other']):
    """
    Create and plot a normalized confusion matrix.

    Parameters
    ----------
    predicted   : Tuple
        Contaings all predicted values for each classifier.
        NOTE:
            Also contains target test values at index 0

    labels  : list
        List of genre labels

    Returns
    -------
    Consufion Matrix
    """
    targets = predicted[0]
    classifiers = ['MultinomialNB', 'KNN', 'Decision Trees', 'One v Rest']
    for i, j in enumerate(classifiers):
        if (j == 'KNN'):
            k_w = [(5, 'uniform'), (10, 'uniform'), (25, 'uniform'),
                   (50, 'uniform'), (100, 'uniform'), (250, 'uniform'),
                   (5, 'distance'), (10, 'distance'), (25, 'distance'),
                   (50, 'distance'), (100, 'distance'), (250, 'distance')]
            for k, l in enumerate(k_w):
                title = 'KNN (k={}, w={})'.format(l[0], l[1])
                cm = confusion_matrix(targets, predicted[i+1][k])
                cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                plt.figure()
                plot_confusion_matrix(cm_norm, title, labels=labels)
                plt.show()
        else:
            cm = confusion_matrix(targets, predicted[i+1])
            cm_norm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
            plt.figure()
            plot_confusion_matrix(cm_norm, j, labels=labels)
            plt.show()


# Note Cross Validation function not functioning

if __name__ == '__main__':
    test = single_run_even()
    print('done')
    get_confusion_matrix(test)
    # Everything below were tests and experiments that we ran before createing
    # the pipeline functions
    '''
    train_file = 'data/train.json'

    # WITH ALL GENERS
    train, test = setup.set_up_test_on_training(train_file)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    print()
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf = extract_text_features(train['data'], test['data'])
    #
    print('Dimensions of X_train_counts are', X_train_counts.shape)
    print()
    num_non_zero = X_train_counts.nnz
    av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
    av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
    #
    print()
    print('Number of non-zero elements in X_train_counts:', num_non_zero)
    print('Average number of word tokens per document:', "%.4f" % av_num_word_tokens_per_doc)
    print('Average number of documents per word token:', "%.4f" % av_num_docs_per_word_token)
    print()

    # MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)

    # # Print out the results to see how they are classified.
    # # Multinomial naive Bayes
#    for doc, category in zip(test['data'], predicted_multNB):
#        print('%r => %s' % (doc, twenty_train.target_names[category]))
#    print()
    #
    print('Accuracy with multinomial naive Bayes: ', '%.4f'
          % np.mean(predicted_multNB == test_targets) )
    #

    # KNN
    k = [5, 10, 25, 50, 100, 250]
    w = ['uniform', 'distance']
    print('Accuracy with k-nearest neighbors')
    for i in k:
        for j in w:
            predicted_knn = fit_and_predict_knn(
                            X_train_tfidf, train_targets, X_test_tfidf,
                            i, j)
            print('\tk={}, w={}: '.format(i, j), '%.4f' % np.mean(predicted_knn == test_targets) )

    # Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_dtree == test_targets))

    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with One v Rest: ', '%.4f'
          % np.mean(predicted_ovr == test_targets))


    ##############################################################
    # WITH EVEN Genres
    genre_list = ['Christian & Gospel', 'Country', 'Electronic', 'Hip-Hop/Rap',
                  'Jazz & Blues', 'Metal', 'Pop', 'RnB/Soul', 'Rock', 'Other']

    genre_list2 = ['Country', 'Hip-Hop/Rap',
                   'Metal', 'Pop', 'RnB/Soul', 'Rock']
    genre_list3 = ['Country', 'Hip-Hop/Rap']
    genre_list4 = ['Hip-Hop/Rap', 'Rock']
    genre_list5 = ['Country', 'Rock']
    genre_list6 = ['RnB/Soul', 'Hip-Hop/Rap']
    train, test = setup.set_up_even_genres(train_file, genre_list)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    print()
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf = extract_text_features(train['data'], test['data'])
    #
    print(len(train['target']), len(test['target']))
    print()
    print('Dimensions of X_train_counts are',X_train_counts.shape)
    print()
    num_non_zero = X_train_counts.nnz
    av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
    av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
    #
    print()
    print('Number of non-zero elements in X_train_counts:', num_non_zero)
    print('Average number of word tokens per document:', "%.4f" % av_num_word_tokens_per_doc)
    print('Average number of documents per word token:', "%.4f" % av_num_docs_per_word_token)
    print()

    # MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)

    # # Print out the results to see how they are classified.
    # # Multinomial naive Bayes
#    for doc, category in zip(test['data'], predicted_multNB):
#        print('%r => %s' % (doc, twenty_train.target_names[category]))
#    print()
    #
    print('Accuracy with multinomial naive Bayes: ', '%.4f'
          % np.mean(predicted_multNB == test_targets) )
    #

    # KNN
    k = [5, 10, 25, 50, 100, 250]
    w = ['uniform', 'distance']
    print('Accuracy with k-nearest neighbors')
    for i in k:
        for j in w:
            predicted_knn = fit_and_predict_knn(
                            X_train_tfidf, train_targets, X_test_tfidf,
                            i, j)
            print('\tk={}, w={}: '.format(i, j), '%.4f' % np.mean(predicted_knn == test_targets) )

    # Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_dtree == test_targets))

    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with One v Rest: ', '%.4f'
          % np.mean(predicted_ovr == test_targets))

    # One vs. One
    predicted_ovo = fit_and_predict_ovo(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with One v One: ', '%.4f'
          % np.mean(predicted_ovo == test_targets))

    ###############################################
    genre_list4 = ['Hip-Hop/Rap', 'Country']
    genre_list5 = ['Hip-Hop/Rap', 'Rock']
    genre_list6 = ['RnB/Soul', 'Hip-Hop/Rap']
    #train, test = setup.set_up_even_genres(train_file, genre_list)
    train_file = 'data/train.json'
    # Hip-Hop/Rap vs. Country
    print()
    print('COMPARING HIP-HOP and COUNTRY')
    print()
    train, test = setup.set_up_even_genres(train_file, genre_list4)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    print()
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf = extract_text_features(train['data'], test['data'])
    #
    print('Dimensions of X_train_counts are',X_train_counts.shape)
    print()
    num_non_zero = X_train_counts.nnz
    av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
    av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
    #
    print()
    print('Number of non-zero elements in X_train_counts:', num_non_zero)
    print('Average number of word tokens per document:', "%.4f" % av_num_word_tokens_per_doc)
    print('Average number of documents per word token:', "%.4f" % av_num_docs_per_word_token)
    print()

    # MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)

    # # Print out the results to see how they are classified.
    # # Multinomial naive Bayes
#    for doc, category in zip(test['data'], predicted_multNB):
#        print('%r => %s' % (doc, twenty_train.target_names[category]))
#    print()
    #
    print('Accuracy with multinomial naive Bayes: ', '%.4f'
          % np.mean(predicted_multNB == test_targets) )

    # KNN
    k = [5, 10, 25, 50, 100, 250]
    w = ['uniform', 'distance']
    print('Accuracy with k-nearest neighbors')
    for i in k:
        for j in w:
            predicted_knn = fit_and_predict_knn(
                            X_train_tfidf, train_targets, X_test_tfidf,
                            i, j)
            print('\tk={}, w={}: '.format(i, j), '%.4f' % np.mean(predicted_knn == test_targets) )

    # Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_dtree == test_targets))

    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_ovr == test_targets))


    # Hip-Hop/Rap vs. Rock
    print()
    print('COMPARING HIP-HOP and COUNTRY')
    print()
    train, test = setup.set_up_even_genres(train_file, genre_list5)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    print()
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf = extract_text_features(train['data'], test['data'])
    #
    print('Dimensions of X_train_counts are',X_train_counts.shape)
    print()
    num_non_zero = X_train_counts.nnz
    av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
    av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
    #
    print()
    print('Number of non-zero elements in X_train_counts:', num_non_zero)
    print('Average number of word tokens per document:', "%.4f" % av_num_word_tokens_per_doc)
    print('Average number of documents per word token:', "%.4f" % av_num_docs_per_word_token)
    print()

    # MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)

    # # Print out the results to see how they are classified.
    # # Multinomial naive Bayes
#    for doc, category in zip(test['data'], predicted_multNB):
#        print('%r => %s' % (doc, twenty_train.target_names[category]))
#    print()
    #
    print('Accuracy with multinomial naive Bayes: ', '%.4f'
          % np.mean(predicted_multNB == test_targets) )

    # KNN
    k = [5, 10, 25, 50, 100, 250]
    w = ['uniform', 'distance']
    print('Accuracy with k-nearest neighbors')
    for i in k:
        for j in w:
            predicted_knn = fit_and_predict_knn(
                            X_train_tfidf, train_targets, X_test_tfidf,
                            i, j)
            print('\tk={}, w={}: '.format(i, j), '%.4f' % np.mean(predicted_knn == test_targets) )

    # Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_dtree == test_targets))

    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_ovr == test_targets))


    # Pop, Metal, Christian
    print()
    print('C 3')
    print()
    train, test = setup.set_up_even_genres(train_file, genre_list6)
    train_targets = np.array(train['target'])
    test_targets = np.array(test['target'])
    print()
    X_train_counts, X_train_tfidf, X_test_counts, X_test_tfidf = extract_text_features(train['data'], test['data'])
    #
    print('Dimensions of X_train_counts are',X_train_counts.shape)
    print()
    num_non_zero = X_train_counts.nnz
    av_num_word_tokens_per_doc = X_train_counts.sum(axis=1).mean()
    av_num_docs_per_word_token = X_train_counts.sum(axis=0).mean()
    #
    print()
    print('Number of non-zero elements in X_train_counts:', num_non_zero)
    print('Average number of word tokens per document:', "%.4f" % av_num_word_tokens_per_doc)
    print('Average number of documents per word token:', "%.4f" % av_num_docs_per_word_token)
    print()

    # MultinomialNB
    predicted_multNB = fit_and_predict_multinomialNB(
                            X_train_tfidf, train_targets, X_test_tfidf)

    # # Print out the results to see how they are classified.
    # # Multinomial naive Bayes
#    for doc, category in zip(test['data'], predicted_multNB):
#        print('%r => %s' % (doc, twenty_train.target_names[category]))
#    print()
    #
    print('Accuracy with multinomial naive Bayes: ', '%.4f'
          % np.mean(predicted_multNB == test_targets) )

    # KNN
    k = [5, 10, 25, 50, 100, 250]
    w = ['uniform', 'distance']
    print('Accuracy with k-nearest neighbors')
    for i in k:
        for j in w:
            predicted_knn = fit_and_predict_knn(
                            X_train_tfidf, train_targets, X_test_tfidf,
                            i, j)
            print('\tk={}, w={}: '.format(i, j), '%.4f' % np.mean(predicted_knn == test_targets) )

    # Decision Tree
    predicted_dtree = fit_and_predict_dtree(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_dtree == test_targets))

    # One vs. Rest
    predicted_ovr = fit_and_predict_ovr(
                            X_train_tfidf, train_targets, X_test_tfidf)
    print('Accuracy with Decision Tree: ', '%.4f'
          % np.mean(predicted_ovr == test_targets))
    '''
