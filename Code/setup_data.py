#!/usr/bin/env python3

""".

setup_data.py

    Creator:
        - Name      : Heliodoro Tejeda
        - E-mail    : htejeda.programming@gmail.com
        - Info      : 2 Mar. 2016

A script to setup the song data to be input into classifiers.
"""

import json
import re
import string

punctuation = string.punctuation + '\n'
# print(punctuation)
# print(type(punctuation))


def load_data(fpath):
    """
    Load the music dataset.

    Parameters
    ----------
    fpath   : str
        File path to the data file we want to open. Must be a json file.

    Returns
    -------
    Dict
        The dataset in dictionary form.
    """
    with open(fpath) as f:
        data = json.load(f)
    return data


def clean_up_lyrics(lyrics):
    """
    Clean up the lyrics of a song.

    Remove punctuation and convert everything to lower case.

    Parameters
    ----------
    lyrics  : str
        Raw lyrics from data

    Returns
    -------
    Str
        Cleaned up version of lyrics
    """
    lyrics = lyrics.lower()
    remove_punct = re.compile('[%s]' % re.escape(punctuation))
    lyrics = remove_punct.sub(' ', lyrics)
    lyrics = ' '.join(lyrics.split())
    return lyrics


def set_up_training(data):
    """
    Set up the training data as a dictionary to be passed to CountVectorizor.

    Parameters
    ----------
    data    : dict
        Dict representation of the training data

    Returns
    -------
    Dict
        {data   : [List of lyrics],
         target : [List of genres]}
    """
    data_dict = {'data': [],
                 'target': []
                 }
    for genre in data.keys():
        for track in data[genre].keys():
            data_dict['data'].append(
                clean_up_lyrics(data[genre][track]['lyrics'])
                )
            data_dict['target'].append(genre)
    return data_dict


def set_up_test_on_training(f_path):
    """
    Split training data so that 80% is used for training and 20% for testing.

    Returns a dictionary for both training and test data.

    Parameters
    ----------
    f_path    : str
        File path to the data we want to load

    Returns
    -------
    Tuple(dict, dict)
        (
        # 80% training data
        {data   : [List of lyrics],
         target : [List of genres]},
        # 20% test data
        {data   : [List of lyrics],
         target : [List of genres]}
        )
    """
    data = load_data(f_path)
    train_dict = {'data': [],
                  'target': []}
    test_dict = {'data': [],
                 'target': []}
    for genre in data.keys():
        track_list = list(data[genre].keys())
        tracks_in_genre = len(track_list)
        percent_cut_off_80 = int(tracks_in_genre*0.8)
        for i in range(tracks_in_genre):
            if (i < percent_cut_off_80):
                train_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                train_dict['target'].append(genre)
            else:
                test_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                test_dict['target'].append(genre)
    return train_dict, test_dict


def set_up_even_genres(f_path, genres):
    """
    Set up the data so that all genres have the same amount of tracks.

    Split training data so that 80% is used for training and 20% for testing.

    Returns a dictionary for both training and test data.

    Parameters
    ----------
    f_path  :   str
        File path to the data we want to load

    genres  :   list
        List of genres to use for classification

    Returns
    -------
    Tuple(dict, dict)
        (
        # 80% training data
        {data   : [List of lyrics],
         target : [List of genres]},
        # 20% test data
        {data   : [List of lyrics],
         target : [List of genres]}
        )
    """
    data = load_data(f_path)
    train_dict = {'data': [],
                  'target': []}
    test_dict = {'data': [],
                 'target': []}
    min_genre = 1000000
    for genre in genres:
        if (len(data[genre].keys()) < min_genre):
            min_genre = len(data[genre].keys())

    for genre in genres:
        track_list = list(data[genre].keys())
        percent_cut_off_80 = int(min_genre*0.8)
        for i in range(min_genre):
            if (i < percent_cut_off_80):
                train_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                train_dict['target'].append(genre)
            else:
                test_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                test_dict['target'].append(genre)
    return train_dict, test_dict


def set_up_even_test_on_rest(f_path, genres, split=0.8):
    """
    Set up the data so that all genres have the same amount of training tracks.

    Split training data so that 80% is used for training and 20% for testing.

    Returns a dictionary for both training and test data.

    Parameters
    ----------
    f_path  :   str
        File path to the data we want to load

    genres  :   list
        List of genres to use for classification

    Returns
    -------
    Tuple(dict, dict)
        (
        # 80% training data
        {data   : [List of lyrics],
         target : [List of genres]},
        # 20% test data
        {data   : [List of lyrics],
         target : [List of genres]}
        )
    """
    data = load_data(f_path)
    train_dict = {'data': [],
                  'target': []}
    test_dict = {'data': [],
                 'target': []}
    min_genre = 1000000
    for genre in genres:
        print(min_genre)
        if (len(data[genre].keys()) < min_genre):
            min_genre = len(data[genre].keys())
    percent_cut_off_80 = int(min_genre*split)
    print('minimum at:', min_genre)
    print(percent_cut_off_80)
    for genre in genres:
        track_list = list(data[genre].keys())
        tracks_per_genre = len(data[genre].keys())
        print(genre, percent_cut_off_80)
        for i in range(tracks_per_genre):
            if (i < percent_cut_off_80):
                train_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                train_dict['target'].append(genre)
            else:
                test_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                test_dict['target'].append(genre)
    return train_dict, test_dict


def set_up_specific_genres(f_path, genres):
    """
    Split training data so that 80% is used for training and 20% for testing.

    Returns a dictionary for both training and test data.

    Parameters
    ----------
    f_path  :   str
        File path to the data we want to load

    genres  :   list
        List of genres to use for classification

    Returns
    -------
    Tuple(dict, dict)
        (
        # 80% training data
        {data   : [List of lyrics],
         target : [List of genres]},
        # 20% test data
        {data   : [List of lyrics],
         target : [List of genres]}
        )
    """
    data = load_data(f_path)
    train_dict = {'data': [],
                  'target': []}
    test_dict = {'data': [],
                 'target': []}
    for genre in genres:
        track_list = list(data[genre].keys())
        tracks_in_genre = len(track_list)
        percent_cut_off_80 = int(tracks_in_genre*0.8)
        print(genre, tracks_in_genre, percent_cut_off_80)
        for i in range(tracks_in_genre):
            if (i < percent_cut_off_80):
                train_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                train_dict['target'].append(genre)
            else:
                test_dict['data'].append(
                    clean_up_lyrics(
                        data[genre][track_list[i]]['lyrics']
                    )
                )
                test_dict['target'].append(genre)
    return train_dict, test_dict


def set_up_test(data):
    """
    Set up the test data as a dictionary to be passed to CountVectorizor.

    Parameters
    ----------
    data    : dict
        Dict representation of the test data

    Returns
    -------
    List
        [List of lyrics]
    """
    data_list = []
    for track in data.keys():
        data_list.append(clean_up_lyrics(data[track]['lyrics']))
    return data_list


def set_up_test_multi_genre(data):
    """
    Set up the test data as a dictionary to be passed to CountVectorizor.

    Parameters
    ----------
    data    : dict
        Dict representation of the test data

    Returns
    -------
    Dict
        {data   : [List of lyrics],
         target : [List of genres]}
    """
    data_dict = {'data': [],
                 'target': []
                 }
    for track in data.keys():
        data_dict['data'].append(clean_up_lyrics(data[track]['lyrics']))
        data_dict['target'].append(data[track]['genres'])
    return data_dict


if __name__ == '__main__':
    # These were tests to make sure each function ran correctly
    train = 'data/train.json'
    train, test = set_up_specific_genres(train, ['Hip-Hop/Rap', 'Country'])
    print(train['data'][:10])
    print(train['target'][:10])
    print(train['target'][-10:])
    print(test['target'][:10])
    print(test['target'][-10:])

    # SUCCESS
    """
    train = 'data/train.json'
    train_data = load_data(train)
    for i in train_data.keys():
        print(i, type(i))
    clean_lyrics = clean_up_lyrics(train_data['Christian & Gospel']['10013115']['lyrics'])
    print(clean_lyrics)
    print(type(clean_lyrics))
    training_data = set_up_training(train_data)
    for t in training_data['data'][:10]: print(t)
    print(training_data['target'][:10])
    print()
    print('Setting up testing on training')
    testing_on_training = set_up_test_on_training(train_data)
    print(len(testing_on_training[0]['target']))
    print(len(testing_on_training[1]['target']))

    test = 'data/raw_data/test_no_genre.json'
    test_multi = 'data/raw_data/test_multi_genre.json'

    test1 = load_data(test)
    test2 = load_data(test_multi)

    t1 = set_up_test(test1)
    t2 = set_up_test_multi_genre(test2)
    print(type(t1))
    print(t1[:10])
    print()
    print(type(t2))
    print(t2['target'][:10])
    print()
    print(len(t1))
    print(len(t2['target']))
    """
