#!/usr/bin/env python3

"""Setup script for pyMXmatch module."""

from distutils.core import setup

setup(name='mxm',
      version='0.5',
      description='Python 3.5 Wrapper for MusiXmatch API',
      author='Heliodoro Tejeda',
      author_email='htejeda.uci@gmail.com',
      packages=['MusiXmatch']
      )
