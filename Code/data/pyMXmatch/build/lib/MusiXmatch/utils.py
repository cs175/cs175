#!/usr/bin/env python3

""".

utils.py
    Creator Info:
        Name    : Heliodoro Tejeda
        e-mail  : htejeda.uci@gmail.com
        Date    : 14 Feb. 2016

Set of utils functions used by the MusiXmatch Python API.
"""

# Built-in modules
import copy
import json
import os
import queue
import sys
import time
import urllib

# Other modules
import requests  # to install --> pip install requests


# details of the website to call
API_HOST = 'api.musixmatch.com'
API_SELECTOR = '/ws/1.1/'


# MusixMatch API key, should be an environment variable
if ('MUSIXMATCH_API_KEY' in os.environ):
    MUSIXMATCH_API_KEY = os.environ['MUSIXMATCH_API_KEY']
else:
    raise ValueError('environment variable "MUSIXMATCH_API_KEY" not set')


# typical API error message
class MusixMatchAPIError(Exception):
    """Error raised when the status code for MusixMatch API is not 200."""

    def __init__(self, code, message=None):
        """init main."""
        self.mxm_code = code
        if message is None:
            message = status_code(code)
        self.args = ('MusixMatch API Error %d: %s' % (code, message),)


# timeout for the API call (seconds)
# raise a URLError if the call takes too long
MXM_CALL_TIMEOUT = 30

# cache time length (seconds)
CACHE_TLENGTH = 3600


# TODO
# Come back and figure out if this is even usefull
# If so, HOW???
class TimedCache():
    """Cach hashable object for a given time length."""

    def __init__(self, verbose=0):
        """contructor, init main dict and priority queue."""
        self.stuff = {}
        self.last_cleanup = time.time()
        self.verbose = verbose

    def cache(self, query, res):
        """
        Cache a query with a given result (res).

        Use the occasion to remove one old stuff if needed
        """
        # remove old stuff
        curr_time = time.time()
        if curr_time - self.last_cleanup > CACHE_TLENGTH:
            if self.verbose:
                print('we cleanup cache')
            new_stuff = {}
            new_stuff.update(filter(
                lambda x: curr_time - x[1][0] < CACHE_TLENGTH,
                self.stuff.items()))
            self.stuff = new_stuff
            self.last_cleanup = curr_time
        # add object to cache (try/except should be useless now)
        try:
            hashcode = hash(query)
            if self.verbose:
                print('cache, hashcode is:', hashcode)
            self.stuff[hashcode] = (time.time(), copy.deepcopy(res))
        except TypeError as e:
            print('Error, stuff not hashable:', e)
            pass

    def query_cache(self, query):
        """
        Query the cache for a given query.

        Return None if not there or too old
        """
        hashcode = hash(query)
        if self.verbose:
            print('query_cache, hashcode is:', hashcode)
        if hashcode in self.stuff.keys():
            print('hashcode in self.stuff.keys()')
            data = self.stuff[hashcode]
            print(data)
            print(data[0])
            if time.time() - data[0] > CACHE_TLENGTH:
                print(self.stuff.pop(hashcode))
                self.stuff.pop(hashcode)
                return None
            return data[1]
        return None


# instace of the cache
MXMPY_CACHE = TimedCache()


def call(method, params, nocaching=False):
    """
    Do the GET call to the MusixMatch API.

    Paramteres
      method     - string describing the method, e.g. track.get
      params     - dictionary of params, e.g. track_id -> 123
      nocaching  - set to True to disable caching
    """
    for k, v in params.items():
        if isinstance(v, str):
            params[k] = v.encode('utf-8')

    # sanity checks
    params['format'] = 'json'
    if (('apikey' not in params.keys()) or (params['apikey'] is None)):
        params['apikey'] = MUSIXMATCH_API_KEY
    if (params['apikey'] is None):
        raise MusixMatchAPIError(-1, 'EMPTY API KEY, NOT IN YOUR ENVIRONMENT?')

    # caching
    if (not nocaching):
        check_params = urllib.parse.urlencode(params)
        cached_res = MXMPY_CACHE.query_cache(method + str(check_params))
        if (cached_res is not None):
            return cached_res

    url = 'http://%s%s%s?' % (API_HOST, API_SELECTOR, method)
    response = requests.get(url, params=params, timeout=MXM_CALL_TIMEOUT)
    res_checked = check_status(response.json())
    # cache
    if (not nocaching):
        MXMPY_CACHE.cache(method + str(check_params), res_checked)
    # done
    return res_checked


def check_status(response):
    """
    Check the response in JSON format.

    Raise an error, or returns the body of the message
    RETURN:
       body of the message in JSON
       except if error was raised
    """
    if ('message' not in response.keys()):
        raise MusixMatchAPIError(-1)
    msg = response['message']
    if ('header' not in msg.keys()):
        raise MusixMatchAPIError(-1)
    header = msg['header']
    if ('status_code' not in header.keys()):
        raise MusixMatchAPIError(-1)
    code = header['status_code']
    if (code != 200):
        raise MusixMatchAPIError(code)
    # all good, return body
    body = msg['body']
    return body


def status_code(value):
    """
    Get a value, i.e. error code as a int.

    Returns an appropriate message.
    """
    if value == 200:
        q = "The request was successful."
        return q
    if value == 400:
        q = "The request had bad syntax or was inherently impossible"
        q += " to be satisfied."
        return q
    if value == 401:
        q = "Authentication failed, probably because of a bad API key."
        return q
    if value == 402:
        q = "A limit was reached, either you exceeded per hour requests"
        q += " limits or your balance is insufficient."
        return q
    if value == 403:
        q = "You are not authorized to perform this operation / the api"
        q += " version you're trying to use has been shut down."
        return q
    if value == 404:
        q = "Requested resource was not found."
        return q
    if value == 405:
        q = "Requested method was not found."
        return q
    # wrong code?
    return "Unknown error code: " + str(value)
