#!/usr/bin/env python3

""".

track.py
    Creator Info:
        Name    : Heliodoro Tejeda
        e-mail  : htejeda.uci@gmail.com
        Date    : 15 Feb. 2016

Track class and functions to query MusiXmatch regarding a track
(find the track, get lyrics, chart info, ...)
"""

# Built-in modules
import os
import sys

# Other modules
import utils


class Track(object):
    """
    Class to query the MusiXmatch API tracks.

    If the class is constructed with a MusiXmatch ID (default), we assume the
    ID exists. The constructor can find the track from a musicbrainz ID or
    Echo Nest track ID. Then, one can search for lyrics or charts.
    """

    # track.get in API
    def __init__(self, track_id, musicbrainz=False,
                 echonest=False, trackdata=None):
        """
        Create a Track object based on given ID.

        If musicbrainz or echonest is True, search for the song. Takes a
        MusiXmatch ID (if both musicbrainz and echonest are False) or
        musicbrainz ID or echonest track id Raises an exception if the track
        is not found.

        INPUT:
            track_id    - track id (from whatever service)
            musicbrainz - set to True if track_id from musicbrainz
            echonest    - set to True if track_id from echonest
            trackdata   - if you already have the information about the track
                          (after a search), bypass API call
        """
        if (musicbrainz and echonest):
            raise ValueError('Creating a Track, only musicbrainz OR echonest' +
                             'can be True')
        if (trackdata is None):
            if (musicbrainz):
                params = {'musicbrainz_id': track_id}
            elif (echonest):
                params = {'echonest_track_id': track_id}
            else:
                params = {'track_id': track_id}
            # url call
            body = utils.call('track.get', params)
            trackdata = body['track']
        # save result
        for k in trackdata.keys():
            self.__setattr__(k, trackdata[k])

    def __str__(self):
        """Pretty printout."""
        return 'MusiXmatch Track: ' + str(self.__dict__)

    # track.lyrics.get in the API
    def lyrics(self):
        """
        Get the lyrics for that track.

        RETURN:
            dictionary containing keys:
                - 'lyrics-body' (main data)
                - 'lyrics-id'
                - 'lyrics-language'
                - 'lyrics-copyright'
                - 'pixel_tracking_url'
                - 'script-tracking_url'
        """
        body = utils.call('track.lyrics.get', {'track_id': self.track_id})
        return body['lyrics']


if __name__ == '__main__':
    # create a track
    try:
        track = Track(4623710)
    except utils.MusixMatchAPIError as e:
        if (e.mxm_code == 401):
            print('yes')
        print('MXM error --> code:', e.mxm_code)
    print('*********** TRACK 4110618 ACQUIRED ************')
    print(track)
    print()
    v = vars(track)
    for i in v:
        print(i)
    print()
    print(track.track_length)
    print()
    print('Song:', track.track_name, 'Artist:', track.artist_name)
    print()
    print(track.lyrics())
    print()
    a = track.primary_genres
    print(len(a['music_genre_list']))
    for i in a['music_genre_list']:
        print(i)
        print()
    b = track.secondary_genres
    print(b)
    print()
    print(track.track_name)
    print(track.artist_id)
    print(track.artist_name)
    print(track.album_id)
    print(track.album_name)
    print(track.primary_genres)
    print(track.track_length)
    print(track.track_share_url)
    print(track.first_release_date)

    # NUMBER 2
    track = Track('1241519')
    print('*********** TRACK 4110618 ACQUIRED ************')
    print(track)
    print()
    v = vars(track)
    for i in v:
        print(i)
    print()
    print(track.track_length)
    print()
    print('Song:', track.track_name, 'Artist:', track.artist_name)
    print()
    print(track.lyrics())
    print()
    a = track.primary_genres
    print(len(a['music_genre_list']))
    for i in a['music_genre_list']:
        print(i)
        print()
    b = track.secondary_genres
    print(b)
