#!/usr/bin/env python3

""".

create_data.py

Some shit i will fill out later.
"""

import track
import json
import requests
from lxml import html
import utils
import os

json_file = 'data.json'
find_genre = 'find_genre.json'
narrow_genre = 'narrow_genre.json'
redo_lyrics = 'redo_lyrics.json'
genres_file = 'genres.json'
error_file = 'error.txt'
id_file = 'ids.txt'
non_en = 'no_en.txt'
again = 'redo.txt'
no_lyrics = 'no_lyrics.txt'
find_lyrics = 'find_lyrics.txt'


def write_to_genres(genre, song_id, info):
    """Write song info to genres json file."""
    with open(genres_file) as f:
        data = json.load(f)
    if (genre not in data.keys()):
        temp_dict = {}
        temp_dict[song_id] = info
        data[genre] = temp_dict
    else:
        data[genre][song_id] = info
    with open(genres_file, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))


def write_to_json(song_id, info):
    """Write song info to json data file."""
    with open(json_file) as f:
        data = json.load(f)
    data[song_id] = info
    with open(json_file, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))


def write_find_genre(song_id, info):
    """Write song info to json data file."""
    with open(find_genre) as f:
        data = json.load(f)
    data[song_id] = info
    with open(find_genre, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))


def write_narrow_genre(song_id, info):
    """Write song info to json data file."""
    with open(narrow_genre) as f:
        data = json.load(f)
    data[song_id] = info
    with open(narrow_genre, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))


def write_redo_lyrics(song_id, info):
    """Write song info to json data file."""
    with open(redo_lyrics) as f:
        data = json.load(f)
    data[song_id] = info
    with open(redo_lyrics, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))


def write_find(song_id):
    """Write song id to error file."""
    with open(find_lyrics, 'a') as f:
        f.write(song_id + '\n')


def write_error(song_id):
    """Write song id to error file."""
    with open(error_file, 'a') as f:
        f.write(song_id + '\n')


def write_redo(song_id):
    """Write song id to redo file."""
    with open(again, 'a') as f:
        f.write(song_id + '\n')


def write_non_en(song_id):
    """Write song id to non en file."""
    with open(non_en, 'a') as f:
        f.write(song_id + '\n')


def write_id(song_id):
    """Write song id to id file."""
    with open(id_file, 'a') as f:
        f.write(song_id + '\n')


def write_no_lyrics(song_id):
    """Write song id to id file."""
    with open(no_lyrics, 'a') as f:
        f.write(song_id + '\n')


def load_id_file():
    """Load the id file."""
    with open('../../mxm_data/mxm_song_ids.txt') as f:
        data = f.read()
        data = data.split('\n')
    return data


def get_track(song_id):
    """Turn song_id info into track info."""
    return track.Track(song_id)


def get_lyrics(cur_track):
    """Get the lyrics of given track."""
    try:
        req = requests.get(cur_track.track_share_url)
        tree = html.fromstring(req.content)
        lyrics = tree.xpath('//span[@id="lyrics-html"]/text()')
        return lyrics[0]
    except:
        write_redo(cur_track.track_id)


def have_lyrics(lyrics):
    """Check if lyrics are not empty list."""
    if (lyrics):
        return True
    else:
        return False


def get_genres(track_genres):
    """Get the track primary genres."""
    genre_list = []
    genres = track_genres['music_genre_list']
    for g in genres:
        genre_list.append(g['music_genre']['music_genre_name'])
    return genre_list


def update_api_key(key_list):
    """Update environment variable to be a new API key."""
    print(key_list)
    new_api = key_list[0]
    print(os.environ["MUSIXMATCH_API_KEY"])
    os.environ["MUSIXMATCH_API_KEY"] = new_api
    print(os.environ["MUSIXMATCH_API_KEY"])
    key_list.remove(new_api)
    return None


API_KEYS = ['69ef604f1305290eca3a8fa65521deaa',
            '656c5ef5a608effb6476693d2fe643b2',
            '6646d2934edea9ee1b69dbf69c9fb389',
            '4d0d207b441fe0cc4c97a314af1f5c1a',
            '61f74398f8ce990295e26847ae4e5672',
            'f91ae236a9f4b87f58b51ed0f3449d4b',
            'fba95bc6d07f4d587007c7f49d919b46',
            '8dccc724ce283cd4092b926c7ed9143f',
            'bfea4f0e711f255a1eed58f27c35d67d',
            'e5a4897a7fdd9761635eb037db4d1311',
            '7d22a00a3bbeb22e60a232e5a98b2582'
            ]


def main(x, y):
    """Main function for getting all of the data."""
    data = load_id_file()
    for i in range(x, y):
        print(i)
        try:
            current = track.Track(data[i])
            if (current.has_lyrics):
                try:
                    lyr = current.lyrics()
                    if (lyr['lyrics_language'] == 'en'):
                        string_lyrics = get_lyrics(current)
                        if (have_lyrics(string_lyrics)):
                            try:
                                genres = get_genres(current.primary_genres)
                                track_data = {
                                    'track_id': data[i],
                                    'track_name': current.track_name,
                                    'artist_id': current.artist_id,
                                    'artist_name': current.artist_name,
                                    'album_id': current.album_id,
                                    'album_name': current.album_name,
                                    'genres': genres,
                                    'length': current.track_length,
                                    'url': current.track_share_url,
                                    'release_date': current.first_release_date,
                                    'lyrics': string_lyrics}
                                write_id(data[i])
                                if (len(string_lyrics.split()) <= 10):
                                    write_redo_lyrics(data[i], track_data)
                                elif (not genres):
                                    write_find_genre(data[i], track_data)
                                elif (len(genres) > 1):
                                    write_narrow_genre(data[i], track_data)
                                else:
                                    write_to_json(data[i], track_data)
                                    write_to_genres(genres[0], data[i],
                                                    track_data)
                            except:
                                write_redo(data[i])
                                print('something went wrong while getting data')
                        else:
                            write_find(data[i])
                    else:
                        write_non_en(data[i])
                except:
                    write_redo(data[i])
            else:
                write_no_lyrics(data[i])
        except utils.MusixMatchAPIError as e:
            if (e.mxm_code == 401 or e.mxm_code == 402):
                write_redo(data[i])
                update_api_key(API_KEYS)
                print(os.environ["MUSIXMATCH_API_KEY"])
                print('switched to a new API key')
            else:
                write_error(data[i])
        except:
            write_error(data[i])
    return None


if __name__ == '__main__':
    # Done up to 55110
    main(55110, 175000)
