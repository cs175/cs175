#!/usr/bin/env python3

""".

genre_mapping.py

    Contributor:
        Name    : Heliodoro Tejeda
        Email   : htejeda.uci@gmail.com
        Date    : 3 March 2016

Mapping script for music genres. This will take a large variety of music
genres and converge them down to the most common genres. In particular,
it will map subgenres of music to their parent genre.

    i.e.    1)  Honky Tonk      ---->   Country
            2)  Dirty South     ---->   Hip-Hop/Rap
            3)  Christian Rock  ----->  Christian & Gospel
"""

import json

IGNORE = set([
        'Alternative & Rock in Spanish', 'Comedy', 'Standards',
        'Singer/Songwriter', 'Soundtrack', 'Pop in Spanish', 'Vocal',
        'French Pop', 'Classical Crossover', 'Pop/Rock', 'Rock & Roll'
        ])

Other = set([
        'Christmas', 'Reggae', 'Salsa y Tropical',
        'Americana', 'Classical', 'World', 'Europe', 'Modern Era',
        'New Age', 'Brazillian', 'Tribute', 'Traditional Folk', 'Latin',
        'Psychedelic', 'Industrial', "Children's Music", 'Easy Listening',
        'New Wave', 'Spoken Word', 'Polka', 'Celtic', 'Holiday'
        ])

Christian_Gospel = set([
        'Christian Metal', 'Christian Rock', 'Gospel', 'Contemporary Gospel',
        'Christian', 'CCM', 'Contemporary Christian Music',
        'Contemporary Christian', 'Christian & Gospel'
        ])

Country = set([
        'Contemporary Country', 'Honky Tonk', 'Bluegrass', 'Urban Cowboy',
        'Country'
        ])

Electronic = set([
        'Electronic', 'Electronica', 'Techno', 'House', 'Dance'
        ])

Hip_Hop_Rap = set([
        'Gangsta Rap', 'Rap', 'West Coast Rap', 'Hip-Hop', 'Hip Hop',
        'Hardcore Rap', 'Dirty South', 'Hip Hop/Rap'
        ])

Jazz_Blues = set([
        'Jazz', 'Bebop', 'Vocal Jazz', 'Blues'
        ])

Metal = set([
        'Heavy Metal', 'Hair Metal', 'Heavy-Metal', 'Hair-Metal',
        'Death-Metal', 'Death Metal', 'Black-Metal', 'Black Metal',
        'Death Metal/Black Metal'
        ])

Pop = set([
        'Pop', 'Teen Pop', 'Vocal Pop'
        ])

RnB_Soul = set([
        'RnB/Soul', 'Soul', 'R & B', "R 'n' B", 'RnB', 'R&B/Soul'
        ])

Rock = set([
        'Rock', 'Soft Rock', 'Soft-Rock', 'Indie-Rock', 'Indie Rock',
        'American Trad Rock', 'Hard Rock', 'Hard-Rock', 'Glam-Rock',
        'Glam Rock', 'Adult Alternative', 'Alternative', 'College Rock',
        'Punk', 'Folk Rock', 'Folk-Rock', 'Prog-Rock', 'Prog Rock',
        'Art Rock', 'Prog-Rock/Art Rock', 'Progressive Rock', 'Art-Rock',
        'College-Rock', 'Arena Rock', 'Arena-Rock', 'Britpop'
        ])

training_data = 'raw_data/train_genres.json'
main_data = 'train.json'

current_file_genres = set([
        'Prog-Rock/Art Rock', 'House', 'Country', 'Christmas', 'Jazz',
        'Traditional Folk', 'Honky Tonk', 'Bluegrass', 'Modern Era',
        'Classical', 'Gangsta Rap', 'Arena Rock', 'American Trad Rock',
        'Contemporary Gospel', 'Rock', 'Classical Crossover',
        'Alternative & Rock in Spanish', 'Urban Cowboy', 'Celtic', 'Techno',
        'Holiday', 'Christian Metal', 'Heavy Metal', 'Indie Rock',
        'Christian & Gospel', 'Reggae', 'R&B/Soul', 'CCM', 'Polka',
        'Death Metal/Black Metal', 'Hard Rock', 'Pop in Spanish',
        'Vocal Jazz', 'Latin', 'Hardcore Rap', 'Vocal', 'Tribute',
        'Singer/Songwriter', 'Electronica', 'Hip Hop/Rap', 'Folk', 'Dance',
        'Rap', 'Adult Alternative', 'Americana', 'Britpop',
        'Salsa y Tropical', 'College Rock', 'Contemporary Country', 'Gospel',
        'Easy Listening', 'Electronic', 'Comedy', 'Pop', 'Alternative',
        'Folk-Rock', 'Vocal Pop', 'New Wave', 'Punk', 'Dirty South',
        'Glam Rock', 'Brazilian', 'Bebop', 'West Coast Rap', 'Hair Metal',
        'Standards', 'New Age', 'Hip-Hop', 'Christian Rock', 'Industrial',
        'Spoken Word', 'Soul', 'Blues', 'Rock & Roll', 'World',
        "Children's Music", 'Psychedelic', 'French Pop', 'Soundtrack',
        'Europe', 'Teen Pop', 'Pop/Rock', 'Soft Rock'
        ])

genres = ['Christian & Gospel', 'Country', 'Electronic', 'Hip-Hop/Rap',
          'Jazz & Blues', 'Metal', 'Pop', 'RnB/Soul', 'Rock', 'Other']


def load_data(fp):
    """
    Load the file specified.

    Parameters
    ----------
    fp  : str
        Path to file you want to open

    Returns
    -------
    Dict
        Dict representation of loaded data
    """
    with open(fp) as f:
        data = json.load(f)
    return data


def map_genres(data):
    """
    Will map all genres to their appropriate place.

    Parameters
    ----------
    data    : Dict
        Dict representation of the data we want to organize

    Returns
    -------
    Dict
        Organized dict representation of the data
    """
    organized_data = {}
    for g in genres:
        organized_data[g] = {}
    data_genres = data.keys()
    for g in data_genres:
        if (g in genres):
            organized_data[g].update(data[g])
        else:
            if (g in Christian_Gospel):
                organized_data['Christian & Gospel'].update(data[g])
            elif (g in Country):
                organized_data['Country'].update(data[g])
            elif (g in Electronic):
                organized_data['Electronic'].update(data[g])
            elif (g in Hip_Hop_Rap):
                organized_data['Hip-Hop/Rap'].update(data[g])
            elif (g in Jazz_Blues):
                organized_data['Jazz & Blues'].update(data[g])
            elif (g in Metal):
                organized_data['Metal'].update(data[g])
            elif (g in Pop):
                organized_data['Pop'].update(data[g])
            elif (g in RnB_Soul):
                organized_data['RnB/Soul'].update(data[g])
            elif (g in Rock):
                organized_data['Rock'].update(data[g])
            elif (g in IGNORE):
                pass
            else:
                organized_data['Other'].update(data[g])
    return organized_data


def create_file(data, fp):
    """
    Create a new updated file at the specified location.

    Parameters
    ----------
    data    : Dict
        Dictionary representation of the data we want in our new file

    fp    : str
        Path to file we want to create

    Returns
    -------
    None
    """
    with open(fp, 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))
    return None

if __name__ == '__main__':
    data = load_data(training_data)
    data = map_genres(data)
    create_file(data, main_data)
