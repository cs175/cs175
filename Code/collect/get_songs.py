#!/usr/bin/env python3

"""CS 175 - Project in AI."""
# Author: Helio Tejeda

import requests
from lxml import html

import string
import re
# funct remove punct
remove_punct = re.compile('[%s]' % re.escape(string.punctuation))

website = 'http://www.songfacts.com/released-1980-1.php'
def gen_url():
    """Will generate a URL."""
    pass


def get_artist(artist):
    """Get the artist desired."""
    if (not artist):
        raise ValueError("An artist was not specified")
    artist = remove_punct.sub('', artist)
    artist = artist.lower().strip().replace(' ', '-')
    return artist


def get_song(song):
    """Get the song desired."""
    pass


if __name__ == '__main__':
    print(get_artist('  '))
