#!/usr/bin/env python3

""".

lyrics_to_bow.py

    Creator:
        - Name      : Heliodoro Tejeda
        - E-mail    : htejeda.programming@gmail.com
        - Info      : 27 Feb. 2016

A script to convert song lyrics into bag-of-words format. This will be used
for the project.
"""

import re
from collections import Counter
from nltk import FreqDist
from nltk.corpus import stopwords
import string

punctuation = string.punctuation.replace("\'", '\n')
stopwords = stopwords.words('english')


def tokenize(lyrics):
    """
    Perform a custom for of tokenization on a raw input of lyrics.

    Parameters
    ----------
    lyrics  : str
        A string containing the full raw lyrics.

    Returns
    -------
    List of strs
        One string for each toke in the lyrics, in the order they appear in
            the lyrics.
    """
    lyrics = lyrics.lower()
    remove_punct = re.compile('[%s]' % re.escape(punctuation))
    lyrics = remove_punct.sub(' ', lyrics)
    lyrics = ' '.join(lyrics.split())
    return lyrics.split()


def remove_stopwords(tokens, words):
    """
    Remove a static set of "english" stopwords from the nltk corpus.

    This function should remove the 100 most common English words.

    Parameters
    ----------
    tokens  : Iterable[str]
        The tokens from which to remove stopwords

    words   : Iterable[str]
        The English tokens to remove.

    Returns
    -------
    List[str]
        The input tokens with stopwords removed.
    """
    words = set(words)
    tokens_wout_stop = [t for t in tokens if t not in words]
    return tokens_wout_stop


def get_bag_of_words(tokens, c_type=False):
    """
    Return a bag-of-words represenation of the given tokens.

    The function has the ability to return the bag-of-words in either of
    two forms: FreqDist or Counter.

    Parameters
    ----------
    tokens  : Iterable[str]
        The tokens to convert to bag-of-words

    c_type  : Boolean
        Default False

    Returns
    -------
    FreqDist or Counter
        The bag-of-words count for each token
    """
    if (c_type):
        return Counter(tokens)
    return FreqDist(tokens)


def bow_pipeline_with_stopwords(lyrics):
    """
    Use the functions tokenize and get_bag_of_words.

    Parameters
    ----------
    lyrics  : str
        A string containing the full raw lyrics.

    Returns
    -------
    Counter
        A bag-of-words representation of the given lyrics.
    """
    tokens = tokenize(lyrics)
    return get_bag_of_words(tokens)


def bow_pipline_custom(lyrics, words, english=True):
    """
    Use all of the functions tokenize, remove_stopwords, and get_bag_of_words.

    Stopwords used in remove_stopwords are custom stopwords. The english
    parameter is by default True, thusly, the english stopwords will also
    be removed.

    Parameters
    ----------
    lyrics  : str
        A string containing the full raw lyrics.

    words   : Iterable[str]
        The English tokens to remove.

    english : Boolean
        True by default. If set to false will not remove english stopwords
        from lyrics.

    Returns
    -------
    Counter
        A bag-of-words representation of the given lyrics.
    """
    tokens = tokenize(lyrics)
    tokens = remove_stopwords(tokens, words)
    if (english):
        tokens = remove_stopwords(tokens, stopwords)
    return get_bag_of_words(tokens)


def bow_pipeline(lyrics):
    """
    Use all of the functions tokenize, remove_stopwords, and get_bag_of_words.

    Parameters
    ----------
    lyrics  : str
        A string containing the full raw lyrics.

    Returns
    -------
    Counter
        A bag-of-words representation of the given lyrics.
    """
    tokens = tokenize(lyrics)
    tokens = remove_stopwords(tokens, stopwords)
    return get_bag_of_words(tokens)
