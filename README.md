# CS 175 - Project in AI

- - - -

### Contributors:
* Brad Rafferty
* Darpan Patel
* Helio Tejeda

- - - -
- - - -

This repo will serve as the host for all of the entire project (Proposal, Final report, and ALL code). Any information regarding the project can be found through here. Links to the Google Docs will also be included on here.

### Proposal
Due Friday (1/29) @ 10 pm

### Final Report
Due XXXXX

### Code
Any information regarding the code will go here. This should be updated as needed.
